package edu.ntnu.stud.model.vector;

import edu.ntnu.stud.model.linalg.Vector2D;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Vector2DTest {

  @Test
  public void testGetters() {
    Vector2D v = new Vector2D(1, 2);
    assertEquals(1, v.getX());
    assertEquals(2, v.getY());
  }

  @Test
  public void negative_testGetters() {
    Vector2D v = new Vector2D(1, 2);
    assertNotEquals(3, v.getX());
    assertNotEquals(4, v.getY());
  }

  @Test
  public void testAdd() {
    Vector2D v1 = new Vector2D(1, 2);
    Vector2D v2 = new Vector2D(3, 4);
    Vector2D v3 = v1.addition(v2);

    Vector2D expectedResult = new Vector2D(4, 6);
    assertEquals(expectedResult.getX(), v3.getX());
    assertEquals(expectedResult.getY(), v3.getY());
  }

  @Test
  public void negative_testAdd() {
    Vector2D v1 = new Vector2D(1, 2);
    Vector2D v2 = new Vector2D(3, 4);
    Vector2D v3 = v1.addition(v2);

    Vector2D wrongResult = new Vector2D(5, 7);
    assertNotEquals(wrongResult.getX(), v3.getX());
    assertNotEquals(wrongResult.getY(), v3.getY());
  }

  @Test
  public void testSub() {
    Vector2D v1 = new Vector2D(1, 2);
    Vector2D v2 = new Vector2D(3, 4);
    Vector2D v3 = v1.subtract(v2);

    Vector2D expectedResult = new Vector2D(-2, -2);
    assertEquals(expectedResult.getX(), v3.getX());
    assertEquals(expectedResult.getY(), v3.getY());
  }

  @Test
  public void negative_testSub() {
    Vector2D v1 = new Vector2D(1, 2);
    Vector2D v2 = new Vector2D(3, 4);
    Vector2D v3 = v1.subtract(v2);

    Vector2D wrongResult = new Vector2D(-3, -3);
    assertNotEquals(wrongResult.getX(), v3.getX());
    assertNotEquals(wrongResult.getY(), v3.getY());
  }
}