package edu.ntnu.stud.model.vector;

import org.apache.commons.math3.complex.Complex;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ApacheComplexTest {

  @Test
  public void testComplex() {
    Complex z = new Complex(3, 4);
    assertEquals(3, z.getReal());
    assertEquals(4, z.getImaginary());
  }

  @Test
  public void testComplex_Sqrt() {
    // result after subtraction
    double realPart = 0.74543;
    double imaginaryPart = -0.11301;

    Complex z = new Complex(realPart, imaginaryPart);
    Complex sqrtZ = z.sqrt();

    assertEquals(0.8658457446544908, sqrtZ.getReal());
    assertEquals(-0.06525989224852967, sqrtZ.getImaginary());
    System.out.println(sqrtZ);
  }
}
