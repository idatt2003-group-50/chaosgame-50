package edu.ntnu.stud.model;

import edu.ntnu.stud.controller.FileHandler;
import edu.ntnu.stud.exception.DescriptionException;
import edu.ntnu.stud.model.linalg.Vector2D;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;


class DescriptionTest {
  private Description sierDescription;
  private Description barnDescription;
  private Description juliDescription;
  private Description mandDescription;
  private Description custDescription;
  private Description failDescription;
  String sierPath = "src/test/java/edu/ntnu/stud/resources/sierTest.txt";
  String juliPath = "src/test/java/edu/ntnu/stud/resources/juliaSet2.txt";
  String mandPath = "src/test/java/edu/ntnu/stud/resources/mandelbrotTest.txt";
  String barnPath = "src/test/java/edu/ntnu/stud/resources/barnTest.txt";
  String custPath = "src/test/java/edu/ntnu/stud/resources/customFractalTest.txt";
  String illegalPath = "src/test/java/edu/ntnu/stud/resources/invalidDescription.txt";

  @BeforeEach
  void setUp() throws DescriptionException {
    sierDescription = FileHandler.readToDescription(sierPath);
    barnDescription = FileHandler.readToDescription(barnPath);
    juliDescription = FileHandler.readToDescription(juliPath);
    mandDescription = FileHandler.readToDescription(mandPath);
    custDescription = FileHandler.readToDescription(custPath);
  }

  @AfterEach
  void tearDown() {
    sierDescription = null;
    barnDescription = null;
    juliDescription = null;
    mandDescription = null;
    custDescription = null;
  }

  @Test
  void negative_testCreateDescription() {
    assertNotNull(sierDescription);
    assertNotNull(barnDescription);
    assertNotNull(juliDescription);
    assertNotNull(mandDescription);
    assertNotNull(custDescription);
  }

  @Test
  void failTest_illegalDescription() {
    assertThrows(DescriptionException.class, () -> FileHandler.readToDescription(illegalPath));
    assertNull(failDescription);
  }

  @Test
  void testDescriptionType() {
    String sierType = sierDescription.getTransformationType();
    String barnType = barnDescription.getTransformationType();
    String juliType = juliDescription.getTransformationType();
    String mandType = mandDescription.getTransformationType();

    assertEquals("Affine2D", sierType);
    assertEquals("Affine2D", barnType);
    assertEquals("Julia", juliType);
    assertEquals("Mandel", mandType);
  }

  @Test
  void negative_testDescriptionType() {
    String sierType = sierDescription.getTransformationType();
    String barnType = barnDescription.getTransformationType();
    String juliType = juliDescription.getTransformationType();
    String mandType = mandDescription.getTransformationType();

    assertNotEquals("Julia", sierType);
    assertNotEquals("Mandel", barnType);
    assertNotEquals("Affine2D", juliType);
    assertNotEquals("Affine2D", mandType);
  }

  @Test
  void testGetCoords() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(1, 1);
    Description description = new Description(null, "abc", minCoords, maxCoords);

    assertEquals(minCoords, description.getMinCoords());
    assertEquals(maxCoords, description.getMaxCoords());
  }

  @Test
  void testSetCoords() {
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(1, 1);
    Description description = new Description(null, "abc", minCoords, maxCoords);
    Vector2D newMin = new Vector2D(2, 2);
    Vector2D newMax = new Vector2D(3, 3);

    description.setMinCoords(newMin);
    description.setMaxCoords(newMax);

    assertEquals(newMin, description.getMinCoords());
    assertEquals(newMax, description.getMaxCoords());
  }

  @Test
  void setCoordsUnchangedLimit() {
    Vector2D initialLimitMin = sierDescription.getLimitMin();
    Vector2D initialLimitMax = sierDescription.getLimitMax();

    Vector2D newMin = new Vector2D(-20, -20);
    Vector2D newMax = new Vector2D(13, 37);
    sierDescription.setMinCoords(newMin);
    sierDescription.setMaxCoords(newMax);

    assertEquals(initialLimitMin, sierDescription.getLimitMin());
    assertEquals(initialLimitMax, sierDescription.getLimitMax());
  }

  @Test
  void negative_setCoordsUnchangedLimit() {
    Vector2D newMin = new Vector2D(-20, -20);
    Vector2D newMax = new Vector2D(13, 37);
    sierDescription.setMinCoords(newMin);
    sierDescription.setMaxCoords(newMax);

    assertNotEquals(newMin, sierDescription.getLimitMin());
    assertNotEquals(newMax, sierDescription.getLimitMax());
  }

  @Test
  void testReadCoords() {
    double expectedSierMinX = 0.0;
    double expectedJuliMinX = -2.0;
    double expectedMandMinX = -2.0;

    assertEquals(expectedSierMinX, sierDescription.getMinCoords().getX());
    assertEquals(expectedJuliMinX, juliDescription.getMinCoords().getX());
    assertEquals(expectedMandMinX, mandDescription.getMinCoords().getX());
  }

  @Test
  void negative_testReadCoords() {
    assertNotEquals(3, juliDescription.getMinCoords().getX());
    assertNotEquals(4, mandDescription.getMinCoords().getX());
    assertNotEquals(5, sierDescription.getMinCoords().getX());
  }
}