package edu.ntnu.stud.model.matrix;

import edu.ntnu.stud.model.linalg.AffineTransform2D;
import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Vector2D;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class AffineTransform2DTest {
  @Test
  public void testAffineTransformation() {
    Matrix2x2 matrix = new Matrix2x2(2, 3, 4, 5);
    Vector2D vector = new Vector2D(7, 11); // Translation by (1, 1)
    AffineTransform2D transform = new AffineTransform2D(matrix, vector);
    Vector2D point = new Vector2D(17, 19); // Original point (2, 2)

    Vector2D transformedPoint = transform.transform(point);

    Vector2D expectedResult = new Vector2D(2*17 + 3*19 + 7, 4*17 + 5*19 + 11);

    assertEquals(transformedPoint.getX(), expectedResult.getX());
    assertEquals(transformedPoint.getY(), expectedResult.getY());
  }

  @Test
  public void negative_testAffineTransformation() {
    Matrix2x2 matrix = new Matrix2x2(2, 3, 4, 5);
    Vector2D vector = new Vector2D(7, 11); // Translation by (1, 1)
    AffineTransform2D transform = new AffineTransform2D(matrix, vector);
    Vector2D point = new Vector2D(17, 19); // Original point (2, 2)

    Vector2D transformedPoint = transform.transform(point);

    Vector2D wrongResult = new Vector2D(1, 1);

    assertNotEquals(transformedPoint.getX(), wrongResult.getX());
    assertNotEquals(transformedPoint.getY(), wrongResult.getY());
  }

  @Test
  public void testOutputVectorFirstTransform() {
    Matrix2x2 matrix = new Matrix2x2(0.5, 1, 1, 0.5);
    Vector2D vector = new Vector2D(3, 1); // b
    AffineTransform2D transform = new AffineTransform2D(matrix, vector);
    Vector2D point = new Vector2D(1,2 );

    Vector2D transformedPoint = transform.transform(point);
    System.out.println(transformedPoint.getX());
    System.out.println(transformedPoint.getY());
  }
}