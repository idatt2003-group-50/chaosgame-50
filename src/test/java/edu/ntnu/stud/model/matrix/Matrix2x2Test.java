package edu.ntnu.stud.model.matrix;

import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Vector2D;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class Matrix2x2Test {

  @Test
  public void testGetters() {
    Matrix2x2 a = new Matrix2x2(1, 2, 3, 4);
    assertEquals(1, a.getA00());
    assertEquals(2, a.getA01());
    assertEquals(3, a.getA10());
    assertEquals(4, a.getA11());
  }

  @Test
  public void negative_testGetters() {
    Matrix2x2 a = new Matrix2x2(1, 2, 3, 4);
    assertNotEquals(5, a.getA00());
    assertNotEquals(6, a.getA01());
    assertNotEquals(7, a.getA10());
    assertNotEquals(8, a.getA11());
  }

  @Test
  public void testMultiply() {
    Matrix2x2 a = new Matrix2x2(1, 2, 3, 4);
    Vector2D v = new Vector2D(1, 2);
    Vector2D result = a.multiply(v);

    Vector2D expectedResult = new Vector2D(5, 11);
    assertEquals(expectedResult.getX(), result.getX());
    assertEquals(expectedResult.getY(), result.getY());
  }

  @Test
  public void negative_testMultiply() {
    Matrix2x2 a = new Matrix2x2(1, 2, 3, 4);
    Vector2D v = new Vector2D(1, 2);
    Vector2D result = a.multiply(v);

    Vector2D wrongResult = new Vector2D(6, 12);
    assertNotEquals(wrongResult.getX(), result.getX());
    assertNotEquals(wrongResult.getY(), result.getY());
  }
}
