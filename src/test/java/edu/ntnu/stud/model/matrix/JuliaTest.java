package edu.ntnu.stud.model.matrix;

import edu.ntnu.stud.controller.FileHandler;
import edu.ntnu.stud.exception.DescriptionException;
import edu.ntnu.stud.model.Description;
import edu.ntnu.stud.model.FractalCanvas;
import edu.ntnu.stud.model.linalg.JuliaTransform;
import edu.ntnu.stud.model.linalg.Transform2D;
import edu.ntnu.stud.model.linalg.Vector2D;
import org.junit.jupiter.api.Test;

import java.util.*;

import static edu.ntnu.stud.model.FractalCanvasTest.printArrayRotated;

public class JuliaTest {

  boolean isPointOutsideDomain(Vector2D point, Description description) {
    return point.getX() > description.getMaxCoords().getX() ||
        point.getY() > description.getMaxCoords().getY() ||
        point.getX() < description.getMinCoords().getX() ||
        point.getY() < description.getMinCoords().getY();
  }

  boolean isRepeatingPattern(List<Vector2D> points) {
    double epsilon = 1e-10; // Define a small epsilon value

    Vector2D lastPoint = points.get(points.size() - 1);
    for (int i = 1; i <= points.size() / 2; i++) {
      Vector2D previousPoint = points.get(points.size() - i);
      Vector2D previousPreviousPoint = points.get(points.size() - 2 * i);

      // Check if the difference between the coordinates is within epsilon
      if ((Math.pow(lastPoint.getX(), 2) - Math.pow(previousPoint.getX(),2)) < epsilon) {
        return true;
      }
    }
    return false;
  }

  void printJuliaArray(double[][] julia) {
    for (int i = 0; i < julia.length; i++) {
      for (int j = 0; j < julia[0].length; j++) {
        System.out.printf("%.2f\t", julia[i][j]);
      }
      System.out.println();
    }
  }

  @Test
  void nonModularJulia() {
    int imWidth = 500;
    int imHeight = 500;
    double[] c = {-0.1, 0.65};
    double zAbsMax = 10;
    int nitMax = 1000;
    double xmin = -1.5;
    double xmax = 1.5;
    double xWidth = xmax - xmin;
    double ymin = -1.5;
    double ymax = 1.5;
    double yHeight = ymax - ymin;

    double[][] julia = new double[imHeight][imWidth];
    for (int ix = 0; ix < imWidth; ix++) {
      for (int iy = 0; iy < imHeight; iy++) {
        int nit = 0;
        // Map pixel position to a point in the complex plane
        double zx = ix / (double) imWidth * xWidth + xmin;
        double zy = iy / (double) imHeight * yHeight + ymin;
        // Do the iterations
        double zxTemp, zyTemp;
        while (Math.sqrt(zx * zx + zy * zy) <= zAbsMax && nit < nitMax) {
          zxTemp = zx * zx - zy * zy + c[0];
          zyTemp = 2 * zx * zy + c[1];
          zx = zxTemp;
          zy = zyTemp;
          nit++;
        }
        double ratio = nit / (double) nitMax;
        julia[iy][ix] = ratio; // Corrected array indexing
      }
    }
  }

  @Test
  void pixelIteration() throws DescriptionException {
    String filePath = "src/test/java/edu/ntnu/stud/resources/juliaSetTest.txt";
    Description description = FileHandler.readToDescription(filePath);

    int width = 500;
    int height = 500;
    FractalCanvas fractalCanvas = new FractalCanvas(width, height, description.getMinCoords(), description.getMaxCoords());

    int iterations = 1000000;
    int count = 0;

    Vector2D initialPoint = new Vector2D(0.0, 0.0);
    Transform2D transformation = description.getTransformations().get(0);
    Random random = new Random();

    double step = (1.9 + 1.9) / width;

    // Initialize the list outside the loop
    List<Vector2D> points = new ArrayList<>();
    double y = -1.9;

    while (y <= 1.9) {
      double x = -1.9; // Reset x coordinate for each new row
      while (x <= 1.9) {
        Vector2D iteratedPoint = transformation.transform(new Vector2D(x, y));
        fractalCanvas.putPixel(iteratedPoint);
        points.add(iteratedPoint);

        count++;
        x += step;
      }
      y += step;
    }
    printArrayRotated("Matrix populated with Julia set:", fractalCanvas.getCanvasArray());
  }


  @Test
  void basedOnWikiCode() throws DescriptionException {
    String filePath = "src/test/java/edu/ntnu/stud/resources/juliaSetTest.txt";
    Description description = FileHandler.readToDescription(filePath);

    int width = 50;
    int height = 50;
    FractalCanvas fractalCanvas = new FractalCanvas(width, height, description.getMinCoords(), description.getMaxCoords());

    double escapeRadius = 10.0; // Choose an escape radius R > 0
    int maxIterations = 1000;

    Transform2D transformation = description.getTransformations().get(0);
    JuliaTransform juliaTransform = (JuliaTransform) transformation;
    Vector2D c = juliaTransform.getC();

    // Iterate through each pixel on the fractalCanvas
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        // Scale x and y coordinates to be between -R and R
        double zx = scaleCoordinate(x, width, escapeRadius);
        double zy = scaleCoordinate(y, height, escapeRadius);

        int iteration = 0;
        double zxTemp;
        while (zx * zx + zy * zy < escapeRadius * escapeRadius && iteration < maxIterations) {
          zxTemp = zx * zx - zy * zy;
          zy = 2 * zx * zy + c.getY();
          zx = zxTemp + c.getX();

          iteration++;
        }

        // Determine the color based on the number of iterations
        int color;
        if (iteration == maxIterations)
          color = 0; // Black
        else
          color = iteration;

        // Draw the pixel on the fractalCanvas
        fractalCanvas.getCanvasArray()[y][x] = color;
      }
    }

    printArrayRotated("Matrix populated with Julia set:", fractalCanvas.getCanvasArray());
  }

  private double scaleCoordinate(int coordinate, int dimensionSize, double escapeRadius) {
    return ((double) coordinate / (double) dimensionSize) * 2 * escapeRadius - escapeRadius;
  }


  @Test
  void squaredTrans_testJulia() throws DescriptionException {
    String filePath = "src/test/java/edu/ntnu/stud/resources/juliaSetTest.txt";
    Description description = FileHandler.readToDescription(filePath);

    int width = 500;
    int height = 500;
    FractalCanvas fractalCanvas = new FractalCanvas(width, height, description.getMinCoords(), description.getMaxCoords());

    int iterations = 1000000;
    int count = 0;

    Transform2D transformation = description.getTransformations().get(0);
    JuliaTransform juliaTransform = (JuliaTransform) transformation;
    Vector2D c = juliaTransform.getC();
    //System.out.println(c.getX());
    //System.out.println(c.getY());

    Random random = new Random();

    Vector2D initialPoint = new Vector2D(1.9, 1.9);
    Vector2D iteratedPoint = transformation.transform(initialPoint);

    // Initialize the list outside the loop
    List<Vector2D> points = new ArrayList<>();

    while (count < iterations) {
      Vector2D randomPoint = new Vector2D(random.nextDouble(description.getMinCoords().getX(), description.getMaxCoords().getX()),
          random.nextDouble(description.getMinCoords().getY(), description.getMaxCoords().getY()));
      iteratedPoint = transformation.transform(randomPoint);
      fractalCanvas.putPixel(iteratedPoint);
      points.add(iteratedPoint);

      //System.out.println("Iteration No. " + count + ", point POS: " + iteratedPoint);


      count++;
    }
    printArrayRotated("Matrix populated with Julia set:", fractalCanvas.getCanvasArray());
  }

  @Test
  void sqrtTrans_testJuliaRepeatingPattern() throws DescriptionException {
    String filePath = "src/test/java/edu/ntnu/stud/resources/juliaSetTest.txt";
    Description description = FileHandler.readToDescription(filePath);

    int width = 50;
    int height = 50;
    FractalCanvas fractalCanvas = new FractalCanvas(width, height, description.getMinCoords(), description.getMaxCoords());

    int iterations = 1000;
    int count = 0;

    Vector2D initialPoint = new Vector2D(0.001, 0.001);

    Transform2D transformation = description.getTransformations().get(0);

    JuliaTransform juliaTransform = (JuliaTransform) transformation;
    Vector2D c = juliaTransform.getC();
    System.out.println(c.getX());
    System.out.println(c.getY());

    Random random = new Random();

    Vector2D iteratedPoint = transformation.transform(initialPoint);

    // Initialize the list outside the loop
    List<Vector2D> points = new ArrayList<>();

    while (count < iterations) {
      iteratedPoint = transformation.transform(iteratedPoint);
      fractalCanvas.putPixel(iteratedPoint);
      points.add(iteratedPoint);
      Vector2D iteratedNegative = iteratedPoint.multiply(-1);
      fractalCanvas.putPixel(iteratedNegative);
      points.add(iteratedNegative);

      System.out.println("Iteration No. " + count + ", point POS: " + iteratedPoint + ", NEG: " + iteratedNegative);

      // Check for repeating pattern
      if (isRepeatingPattern(points)) {
        iteratedPoint = new Vector2D(random.nextDouble(description.getMinCoords().getX(), description.getMaxCoords().getX()),
            random.nextDouble(description.getMinCoords().getY(), description.getMaxCoords().getY()));
      }

      count++;
    }
    printArrayRotated("Matrix populated with Julia set:", fractalCanvas.getCanvasArray());
  }

  @Test
  void testJulia_isThisTheMandelBrotSet() throws DescriptionException {
    String filePath = "src/test/java/edu/ntnu/stud/resources/juliaSetTest.txt";
    Description description = FileHandler.readToDescription(filePath);

    int width = 50;
    int height = 50;
    FractalCanvas fractalCanvas = new FractalCanvas(width, height, description.getMinCoords(), description.getMaxCoords());

    int iterations = 20000;
    int count = 0;

    Vector2D initialPoint = new Vector2D(0, 0);
    Transform2D transformation = description.getTransformations().get(0);

    JuliaTransform juliaTransform = (JuliaTransform) transformation;
    Vector2D c = juliaTransform.getC();
    System.out.println(c.getX());
    System.out.println(c.getY());

    Vector2D iteratedPoint = transformation.transform(initialPoint);

    while (count < iterations) {
      List<Vector2D> points = new ArrayList<>();
      points.add(iteratedPoint);
      points.add(iteratedPoint.multiply(-1));
      System.out.println("Iteration No. "+count + ", point: " + iteratedPoint);
      iteratedPoint = transformation.transform(points.get(new Random().nextInt(points.size())));

      fractalCanvas.putPixel(iteratedPoint);
      count++;
    }
    printArrayRotated("Matrix populated with Julia set:", fractalCanvas.getCanvasArray());

  }
}
