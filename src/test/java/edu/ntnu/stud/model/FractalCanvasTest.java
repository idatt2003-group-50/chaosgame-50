package edu.ntnu.stud.model;

import edu.ntnu.stud.controller.FileHandler;
import edu.ntnu.stud.exception.DescriptionException;
import edu.ntnu.stud.model.linalg.AffineTransform2D;
import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Transform2D;
import edu.ntnu.stud.model.linalg.Vector2D;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

public class FractalCanvasTest {
  private FractalCanvas testCanvas;
  private Description sierDescription;
  private Description juliDescription;
  private FractalCanvas sierCanvas;
  private FractalCanvas juliCanvas;
  private final String sierPath = "src/test/java/edu/ntnu/stud/resources/sierTest.txt";
  private final String juliPath = "src/test/java/edu/ntnu/stud/resources/juliaSet2.txt";

  // Helper method to print a 2D array
  public static void printArray(String message, int[][] array) {
    System.out.println(message);

    int height = array.length;
    int width = array[0].length;

    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        System.out.print(array[i][j] + " ");
      }
      System.out.println();
    }
  }

  // Helper method to print a 2D array
  public static void printArrayRotated(String message, int[][] array) {
    System.out.println(message);

    int height = array.length;
    int width = array[0].length;

    // Iterate over columns instead of rows
    for (int j = 0; j < width; j++) {
      // Iterate over rows in reverse order
      for (int i = height - 1; i >= 0; i--) {
        System.out.print(array[i][j] + " ");
      }
      System.out.println();
    }
  }

  @BeforeEach
  void setUp() throws DescriptionException {
    testCanvas = new FractalCanvas(10, 10, new Vector2D(0, 0), new Vector2D(10, 10));

    sierDescription = FileHandler.readToDescription(sierPath);
    sierCanvas = new FractalCanvas(48, 48, sierDescription.getMinCoords(), sierDescription.getMaxCoords());

    juliDescription = FileHandler.readToDescription(juliPath);
    juliCanvas = new FractalCanvas(48, 48, juliDescription.getMinCoords(), juliDescription.getMaxCoords());

  }

  @AfterEach
  void tearDown() {
    testCanvas = null;
    sierCanvas = null;
    juliCanvas = null;
    sierDescription = null;
    juliDescription = null;
  }

  @Test
  void testPutPixel() {
    testCanvas.putPixel(new Vector2D(5, 5));

    int[][] canvasArray = testCanvas.getCanvasArray();
    assertEquals(1, canvasArray[5][5]);
  }

  @Test
  void negative_testPutPixel() {
    testCanvas.putPixel(new Vector2D(5, 5));

    assertNotNull(testCanvas.getCanvasArray());
  }

  @Test
  void testClearCanvas() {
    testCanvas.putPixel(new Vector2D(5, 5));

    testCanvas.clearCanvas();

    int[][] canvasArray = testCanvas.getCanvasArray();
    for (int[] ints : canvasArray) {
      for (int anInt : ints) {
        assertEquals(0, anInt);
      }
    }
  }

  @Test
  void negative_testClearCanvas() {
    testCanvas.putPixel(new Vector2D(5, 5));

    testCanvas.clearCanvas();

    int[][] canvasArray = testCanvas.getCanvasArray();
    for (int[] ints : canvasArray) {
      for (int anInt : ints) {
        assertNotEquals(1, anInt);
      }
    }
  }

  @Test
  void setCoords() {
    Vector2D newMin = new Vector2D(13, 37);
    Vector2D newMax = new Vector2D(13, 37);

    testCanvas.setMinCoords(newMin);
    testCanvas.setMaxCoords(newMax);

    assertEquals(newMin, testCanvas.getMinCoords());
    assertEquals(newMax, testCanvas.getMaxCoords());
  }

  @Test
  void negative_setCoords() {
    Vector2D initialMin = testCanvas.getMinCoords();
    Vector2D initialMax = testCanvas.getMaxCoords();
    Vector2D newMin = new Vector2D(13, 37);
    Vector2D newMax = new Vector2D(13, 37);

    testCanvas.setMinCoords(newMin);
    testCanvas.setMaxCoords(newMax);

    assertNotEquals(initialMin, testCanvas.getMinCoords());
    assertNotEquals(initialMax, testCanvas.getMaxCoords());
  }

  @Test
  void testUnchangedLimit() {
    Vector2D initialLimitMin = testCanvas.getLimitMin();
    Vector2D initialLimitMax = testCanvas.getLimitMax();

    testCanvas.setMinCoords(new Vector2D(0, 0));
    testCanvas.setMaxCoords(new Vector2D(10, 10));

    assertEquals(initialLimitMin, testCanvas.getLimitMin());
    assertEquals(initialLimitMax, testCanvas.getLimitMax());
  }

  @Test
  void negative_testUnchangedLimit() {
    Vector2D newMin = new Vector2D(13, 37);
    Vector2D newMax = new Vector2D(13, 37);

    testCanvas.setMinCoords(newMin);
    testCanvas.setMaxCoords(newMax);

    assertNotEquals(newMin, testCanvas.getLimitMin());
    assertNotEquals(newMax, testCanvas.getLimitMax());
  }

  @Test
  void testMoveView_Affine() {
    double initialMinX = sierCanvas.getMinCoords().getX();
    double initialMinY = sierCanvas.getMinCoords().getY();
    double initialMaxX = sierCanvas.getMaxCoords().getX();
    double initialMaxY = sierCanvas.getMaxCoords().getY();

    // Centering on the center (unchanged)
    sierCanvas.moveViewAffine(0.5, 0.5);

    // Check if the canvas is centered around the specified coordinates
    assertEquals(initialMinX, sierCanvas.getMinCoords().getX(), 0.001);
    assertEquals(initialMinY, sierCanvas.getMinCoords().getY(), 0.001);
    assertEquals(initialMaxX, sierCanvas.getMaxCoords().getX(), 0.001);
    assertEquals(initialMaxY, sierCanvas.getMaxCoords().getY(), 0.001);
  }

  @Test
  void testMoveView_Julia() {
    double initialMinX = juliCanvas.getMinCoords().getX();
    double initialMinY = juliCanvas.getMinCoords().getY();
    double initialMaxX = juliCanvas.getMaxCoords().getX();
    double initialMaxY = juliCanvas.getMaxCoords().getY();

    // Centering on the center (unchanged)
    juliCanvas.moveViewComplex(0.5, 0.5);

    // Check if the canvas is centered around the specified coordinates
    assertEquals(initialMinX, juliCanvas.getMinCoords().getX(), 0.001);
    assertEquals(initialMinY, juliCanvas.getMinCoords().getY(), 0.001);
    assertEquals(initialMaxX, juliCanvas.getMaxCoords().getX(), 0.001);
    assertEquals(initialMaxY, juliCanvas.getMaxCoords().getY(), 0.001);
  }


  @Test
  void negative_testCenterAffine() {
    sierCanvas.moveViewAffine(0.5, 0.5);

    assertNotEquals(0.5, sierCanvas.getMinCoords().getX());
    assertNotEquals(0.5, sierCanvas.getMinCoords().getY());
    assertNotEquals(0.5, sierCanvas.getMaxCoords().getX());
    assertNotEquals(0.5, sierCanvas.getMaxCoords().getY());
  }

  @Test
  void testCenterUpperRight() {
    sierCanvas.moveViewAffine(1, 0);

    assertEquals(0.5, sierCanvas.getMinCoords().getX(), 0.001);
    assertEquals(0.5, sierCanvas.getMinCoords().getY(), 0.001);
    assertEquals(1.5, sierCanvas.getMaxCoords().getX(), 0.001);
    assertEquals(1.5, sierCanvas.getMaxCoords().getY(), 0.001);
  }

  @Test
  void negative_testCenterUpperRight() {
    double initialMinX = sierCanvas.getMinCoords().getX();
    double initialMinY = sierCanvas.getMinCoords().getY();
    double initialMaxX = sierCanvas.getMaxCoords().getX();
    double initialMaxY = sierCanvas.getMaxCoords().getY();

    sierCanvas.moveViewAffine(1, 0);

    assertNotEquals(initialMinX, sierCanvas.getMinCoords().getX());
    assertNotEquals(initialMinY, sierCanvas.getMinCoords().getY());
    assertNotEquals(initialMaxX, sierCanvas.getMaxCoords().getX());
    assertNotEquals(initialMaxY, sierCanvas.getMaxCoords().getY());
  }

  @Test
  void testResetViewView() {
    Vector2D initialMin = sierCanvas.getMinCoords();
    Vector2D initialMax = sierCanvas.getMaxCoords();
    Vector2D initialLimitMin = sierCanvas.getLimitMin();
    Vector2D initialLimitMax = sierCanvas.getLimitMax();
    Vector2D newMin = new Vector2D(13, 37);
    Vector2D newMax = new Vector2D(13, 37);

    sierCanvas.setMinCoords(newMin);
    sierCanvas.setMaxCoords(newMax);
    sierCanvas.resetView(initialMin, initialMax);

    assertEquals(initialMin, sierCanvas.getMinCoords());
    assertEquals(initialMax, sierCanvas.getMaxCoords());
    assertEquals(initialLimitMin, sierCanvas.getLimitMin());
    assertEquals(initialLimitMax, sierCanvas.getLimitMax());
  }

  @Test
  void negative_testResetViewView() {
    Vector2D initialMin = sierCanvas.getMinCoords();
    Vector2D initialMax = sierCanvas.getMaxCoords();
    Vector2D newMin = new Vector2D(13, 37);
    Vector2D newMax = new Vector2D(13, 37);

    sierCanvas.setMinCoords(newMin);
    sierCanvas.setMaxCoords(newMax);
    sierCanvas.resetView(initialMin, initialMax);

    assertNotEquals(newMin, sierCanvas.getMinCoords());
    assertNotEquals(newMax, sierCanvas.getMaxCoords());
    assertNotEquals(newMin, sierCanvas.getLimitMin());
    assertNotEquals(newMax, sierCanvas.getLimitMax());
  }

  @Test
  public void testCoordinatesToIndex() {
    int width = 48;
    int height = 48;
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(10, 10);
    FractalCanvas fractalCanvas = new FractalCanvas(width, height, minCoords, maxCoords);

    AffineTransform2D result = fractalCanvas.coordinatesToIndex();
    double resultA00 = result.getMatrix().getA00();
    double resultA01 = result.getMatrix().getA01();
    double resultA10 = result.getMatrix().getA10();
    double resultA11 = result.getMatrix().getA11();
    double resultBx = result.getVector().getX();
    double resultBy = result.getVector().getY();

    Matrix2x2 expectedMatrix = new Matrix2x2(0, (height - 1) / (minCoords.getY() - maxCoords.getY()),
        (width - 1) / (maxCoords.getX() - minCoords.getX()), 0);
    Vector2D expectedVector = new Vector2D(((height - 1) * maxCoords.getY()) / (maxCoords.getY() - minCoords.getY()),
        ((width - 1) * minCoords.getX()) / (minCoords.getX() - maxCoords.getX()));
    AffineTransform2D expectedTransform = new AffineTransform2D(expectedMatrix, expectedVector);

    double expectedA00 = expectedTransform.getMatrix().getA00();
    double expectedA01 = expectedTransform.getMatrix().getA01();
    double expectedA10 = expectedTransform.getMatrix().getA10();
    double expectedA11 = expectedTransform.getMatrix().getA11();
    double expectedBx = expectedTransform.getVector().getX();
    double expectedBy = expectedTransform.getVector().getY();

    assertEquals(expectedA00, resultA00);
    assertEquals(expectedA01, resultA01);
    assertEquals(expectedA10, resultA10);
    assertEquals(expectedA11, resultA11);
    assertEquals(expectedBx, resultBx);
    assertEquals(expectedBy, resultBy);
  }

  @Test
  void negative_testCoordinatesToIndex() {
    int width = 48;
    int height = 48;
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(10, 10);
    FractalCanvas fractalCanvas = new FractalCanvas(width, height, minCoords, maxCoords);

    AffineTransform2D result = fractalCanvas.coordinatesToIndex();
    double resultA00 = result.getMatrix().getA00();
    double resultA01 = result.getMatrix().getA01();
    double resultA10 = result.getMatrix().getA10();
    double resultA11 = result.getMatrix().getA11();
    double resultBx = result.getVector().getX();
    double resultBy = result.getVector().getY();

    assertNotEquals(13, resultA00);
    assertNotEquals(37, resultA01);
    assertNotEquals(38, resultA10);
    assertNotEquals(39, resultA11);
    assertNotEquals(40, resultBx);
    assertNotEquals(41, resultBy);
  }

  @Test
  void testSierpinski() throws DescriptionException {
    String filePath = "src/test/java/edu/ntnu/stud/resources/sierTest.txt";
    Description description = FileHandler.readToDescription(filePath);

    int width = 48;
    int height = 48;
    FractalCanvas fractalCanvas = new FractalCanvas(width, height, description.getMinCoords(), description.getMaxCoords());

    int iterations = 10000;
    int count = 0;
    Vector2D initialPoint = new Vector2D(0, 0);
    Transform2D randSelect = description.getTransformations().get(new Random().nextInt(3));
    Vector2D iteratedPoint = randSelect.transform(initialPoint);

    while (count < iterations) {
      Transform2D randSelect2 = description.getTransformations().get(new Random().nextInt(3));
      iteratedPoint = randSelect2.transform(iteratedPoint);
      fractalCanvas.putPixel(iteratedPoint);
      count++;
    }
    printArrayRotated("Matrix populated with Sierpinski triangle:", fractalCanvas.getCanvasArray());
    assertNotNull(fractalCanvas);
  }

  @Test
  void nonModular_CoordsToIndex() {
    int[][] canvas = {
        {0, 0, 0, 0},
        {0, 0, 0, 0}
    };
    Vector2D point = new Vector2D(4, 0);
    Vector2D maxCoords = new Vector2D(1, 1);
    Vector2D minCoords = new Vector2D(0, 0);

    int width = 4;
    int height = 2;
    // Calculate the transformation matrix A

    int M = height;
    int N = width;

    int a00 = 0;
    int a01 = (M - 1) / (int) (minCoords.getY() - maxCoords.getY());
    int a10 = (N - 1) / (int) (maxCoords.getX() - minCoords.getX());
    int a11 = 0;

    int bx = ((M - 1) * (int) maxCoords.getY()) / (int) (maxCoords.getY() - minCoords.getY());
    int by = ((N - 1) * (int) minCoords.getX()) / (int) (minCoords.getX() - maxCoords.getX());

    Matrix2x2 A = new Matrix2x2(a00, a01, a10, a11);
    Vector2D b = new Vector2D(bx, by);
    AffineTransform2D coordinatesToIndex = new AffineTransform2D(A, b);
    Vector2D index = coordinatesToIndex.transform(point);

    int y = (int) Math.round(index.getY());
    int x = (int) Math.round(index.getX());

    // Check if the calculated indices are within bounds of the fractalCanvas
    if (y < height && x < width) {
      // Update the corresponding cell in the fractalCanvas array
      canvas[y][x] = canvas[y][x] + 1;
    }
    printArray("FractalCanvas with point:", canvas);
    assertNotNull(canvas);
  }

  @Test
  void coordinatesToIndex() {
    int[][] canvas = {
        {0, 0, 0, 0},
        {0, 0, 0, 0}
    };
    Vector2D coordinates = new Vector2D(1, 1);
    int x = (int) coordinates.getX();
    int y = (int) coordinates.getY();

    x = Math.max(0, x - 1);
    y = Math.max(0, y - 1);

    canvas[y][x] = 1;
    printArray("FractalCanvas with point:", canvas);
    assertNotNull(canvas);
  }

  /**
   * Syntax of nested arrays in Java.
   */
  @Test
  void arraySyntax() {
    // this point is located at (4, 1) in the cartesian plane
    int[][] array = {
        {0, 0, 0, 0},
        {0, 0, 0, 1}
    };
    int height = array.length;
    int width = array[0].length;

    // array[-y][-x]
    System.out.println(array[1][3]);
    System.out.println(height);
    System.out.println(width);
  }
}