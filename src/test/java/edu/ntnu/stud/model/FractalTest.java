package edu.ntnu.stud.model;

import edu.ntnu.stud.controller.FileHandler;
import edu.ntnu.stud.exception.DescriptionException;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

class FractalTest {
  int width = 48;
  int height = 48;
  public Fractal sierFractal;
  private Fractal barnFractal;
  private Fractal juliFractal;
  private Fractal mandFractal;
  private Description sierDescription;
  private Description barnDescription;
  private Description juliDescription;
  private Description mandDescription;
  private FractalCanvas sierCanvas;
  private FractalCanvas barnCanvas;
  private FractalCanvas juliCanvas;
  private FractalCanvas mandCanvas;
  String sierPath = "src/test/java/edu/ntnu/stud/resources/sierTest.txt";
  String juliPath = "src/test/java/edu/ntnu/stud/resources/juliaSet2.txt";
  String mandPath = "src/test/java/edu/ntnu/stud/resources/mandelbrotTest.txt";
  String barnPath = "src/test/java/edu/ntnu/stud/resources/barnTest.txt";

  // Helper method to print the fractalCanvas array
  public static void printCanvasRotated(String message, FractalCanvas fractalCanvas) {
    System.out.println(message);

    int[][] canvasArray = fractalCanvas.getCanvasArray(); // Access the fractalCanvas array

    int height = fractalCanvas.getHeight();
    int width = fractalCanvas.getWidth();

    // Iterate over columns instead of rows
    for (int j = 0; j < width; j++) {
      // Iterate over rows in reverse order
      for (int i = height - 1; i >= 0; i--) {
        System.out.print(canvasArray[i][j] + " "); // Print the fractalCanvas array element
      }
      System.out.println();
    }
  }

  @BeforeEach
  void setUp() throws DescriptionException {
    sierDescription = FileHandler.readToDescription(sierPath);
    sierCanvas = new FractalCanvas(width, height, sierDescription.getMinCoords(), sierDescription.getMaxCoords());
    sierFractal = new Fractal(sierDescription, sierCanvas);

    barnDescription = FileHandler.readToDescription(barnPath);
    barnCanvas = new FractalCanvas(width, height, barnDescription.getMinCoords(), barnDescription.getMaxCoords());
    barnFractal = new Fractal(barnDescription, barnCanvas);

    juliDescription = FileHandler.readToDescription(juliPath);
    juliCanvas = new FractalCanvas(width, height, juliDescription.getMinCoords(), juliDescription.getMaxCoords());
    juliFractal = new Fractal(juliDescription, juliCanvas);

    mandDescription = FileHandler.readToDescription(mandPath);
    mandCanvas = new FractalCanvas(width, height, mandDescription.getMinCoords(), mandDescription.getMaxCoords());
    mandFractal = new Fractal(mandDescription, mandCanvas);
  }

  @AfterEach
  void tearDown() {
    sierCanvas = null;
    barnCanvas = null;
    juliCanvas = null;
    mandCanvas = null;
    sierDescription = null;
    barnDescription = null;
    juliDescription = null;
    mandDescription = null;
    sierFractal = null;
    barnFractal = null;
    juliFractal = null;
    mandFractal = null;
  }

  @Test
  void testRunSteps_Sierpinksi() {
    int steps = 10000;
    sierFractal.runSteps(steps);

    printCanvasRotated("Matrix populated with Sierpinski triangle:", sierFractal.getCanvas());
    assertNotNull(sierFractal.getCanvas());
  }

  @Test
  void testRunSteps_Barnsley() {
    int steps = 10000;
    barnFractal.runSteps(steps);

    printCanvasRotated("Matrix populated with barnsley fern:", barnFractal.getCanvas());
    assertNotNull(barnFractal.getCanvas());
  }

  @Test
  void testCenter_Affine() {
    double initialMinX = sierFractal.getCanvas().getMinCoords().getX();
    double initialMinY = sierFractal.getCanvas().getMinCoords().getY();
    double initialMaxX = sierFractal.getCanvas().getMaxCoords().getX();
    double initialMaxY = sierFractal.getCanvas().getMaxCoords().getY();

    // Centering on the center (unchanged)
    sierFractal.moveView(0.5, 0.5);

    // Check if the canvas is centered around the specified coordinates
    assertEquals(initialMinX, sierFractal.getCanvas().getMinCoords().getX(), 0.001);
    assertEquals(initialMinY, sierFractal.getCanvas().getMinCoords().getY(), 0.001);
    assertEquals(initialMaxX, sierFractal.getCanvas().getMaxCoords().getX(), 0.001);
    assertEquals(initialMaxY, sierFractal.getCanvas().getMaxCoords().getY(), 0.001);
  }

  @Test
  void testCenter_Julia() {
    double initialMinX = juliFractal.getCanvas().getMinCoords().getX();
    double initialMinY = juliFractal.getCanvas().getMinCoords().getY();
    double initialMaxX = juliFractal.getCanvas().getMaxCoords().getX();
    double initialMaxY = juliFractal.getCanvas().getMaxCoords().getY();

    // Centering on the center (unchanged)
    juliFractal.moveView(0.5, 0.5);

    assertEquals(initialMinX, juliFractal.getCanvas().getMinCoords().getX(), 0.001);
    assertEquals(initialMinY, juliFractal.getCanvas().getMinCoords().getY(), 0.001);
    assertEquals(initialMaxX, juliFractal.getCanvas().getMaxCoords().getX(), 0.001);
    assertEquals(initialMaxY, juliFractal.getCanvas().getMaxCoords().getY(), 0.001);
  }

  @Test
  void testCenter_Mandel() {
    double initialMinX = mandFractal.getCanvas().getMinCoords().getX();
    double initialMinY = mandFractal.getCanvas().getMinCoords().getY();
    double initialMaxX = mandFractal.getCanvas().getMaxCoords().getX();
    double initialMaxY = mandFractal.getCanvas().getMaxCoords().getY();

    // Centering on the center (unchanged)
    mandFractal.moveView(0.5, 0.5);

    assertEquals(initialMinX, mandFractal.getCanvas().getMinCoords().getX(), 0.001);
    assertEquals(initialMinY, mandFractal.getCanvas().getMinCoords().getY(), 0.001);
    assertEquals(initialMaxX, mandFractal.getCanvas().getMaxCoords().getX(), 0.001);
    assertEquals(initialMaxY, mandFractal.getCanvas().getMaxCoords().getY(), 0.001);
  }

  @Test
  void negative_testCenterAffine() {
    sierFractal.moveView(0.5, 0.5);
    assertNotEquals(0.5, sierFractal.getCanvas().getMinCoords().getX());
    assertNotEquals(0.5, sierFractal.getCanvas().getMinCoords().getY());
    assertNotEquals(0.5, sierFractal.getCanvas().getMaxCoords().getX());
    assertNotEquals(0.5, sierFractal.getCanvas().getMaxCoords().getY());
  }

  @Test
  void testCenterUpperRight() {
    sierFractal.moveView(1, 0);
    assertEquals(0.5, sierFractal.getCanvas().getMinCoords().getX(), 0.001);
    assertEquals(0.5, sierFractal.getCanvas().getMinCoords().getY(), 0.001);
    assertEquals(1.5, sierFractal.getCanvas().getMaxCoords().getX(), 0.001);
    assertEquals(1.5, sierFractal.getCanvas().getMaxCoords().getY(), 0.001);
  }

  @Test
  void negative_testCenterUpperRight() {
    double initialMinX = sierFractal.getCanvas().getMinCoords().getX();
    double initialMinY = sierFractal.getCanvas().getMinCoords().getY();
    double initialMaxX = sierFractal.getCanvas().getMaxCoords().getX();
    double initialMaxY = sierFractal.getCanvas().getMaxCoords().getY();

    // Centering on the upper right corner
    sierFractal.moveView(1, 0);

    // Check if the canvas is centered around the specified coordinates
    assertNotEquals(initialMinX, sierFractal.getCanvas().getMinCoords().getX());
    assertNotEquals(initialMinY, sierFractal.getCanvas().getMinCoords().getY());
    assertNotEquals(initialMaxX, sierFractal.getCanvas().getMaxCoords().getX());
    assertNotEquals(initialMaxY, sierFractal.getCanvas().getMaxCoords().getY());
  }

  @Test
  void testZoomIn(){
    // default zoom factor 1.5
    sierFractal.zoomIn();

    assertEquals(0.16, sierFractal.getCanvas().getMinCoords().getX(), 0.01);
    assertEquals(0.16, sierFractal.getCanvas().getMinCoords().getY(), 0.01);
    assertEquals(0.83, sierFractal.getCanvas().getMaxCoords().getX(), 0.01);
    assertEquals(0.83, sierFractal.getCanvas().getMaxCoords().getY(), 0.01);
  }

  @Test
  void negative_testZoomIn() {
    double initialMinX = sierFractal.getCanvas().getMinCoords().getX();
    double initialMinY = sierFractal.getCanvas().getMinCoords().getY();
    double initialMaxX = sierFractal.getCanvas().getMaxCoords().getX();
    double initialMaxY = sierFractal.getCanvas().getMaxCoords().getY();

    sierFractal.zoomIn();

    assertNotEquals(initialMinX, sierFractal.getCanvas().getMinCoords().getX());
    assertNotEquals(initialMinY, sierFractal.getCanvas().getMinCoords().getY());
    assertNotEquals(initialMaxX, sierFractal.getCanvas().getMaxCoords().getX());
    assertNotEquals(initialMaxY, sierFractal.getCanvas().getMaxCoords().getY());
  }

  @Test
  void testZoomIn_setZoomFactor() {
    sierFractal.setZoomFactor(2.0);
    sierFractal.zoomIn();

    assertEquals(0.25, sierFractal.getCanvas().getMinCoords().getX());
    assertEquals(0.25, sierFractal.getCanvas().getMinCoords().getY());
    assertEquals(0.75, sierFractal.getCanvas().getMaxCoords().getX());
    assertEquals(0.75, sierFractal.getCanvas().getMaxCoords().getY());
  }

  @Test
  void testZoomOut(){
    // default zoom factor 1.5
    sierFractal.zoomOut();

    assertEquals(-0.25, sierFractal.getCanvas().getMinCoords().getX(), 0.01);
    assertEquals(-0.25, sierFractal.getCanvas().getMinCoords().getY(), 0.01);
    assertEquals(1.25, sierFractal.getCanvas().getMaxCoords().getX(), 0.01);
    assertEquals(1.25, sierFractal.getCanvas().getMaxCoords().getY(), 0.01);
  }

  @Test
  void testZoomOut_setZoomFactor() {
    sierFractal.setZoomFactor(2.0);
    sierFractal.zoomOut();

    assertEquals(-0.5, sierFractal.getCanvas().getMinCoords().getX(), 0.01);
    assertEquals(-0.5, sierFractal.getCanvas().getMinCoords().getY(), 0.01);
    assertEquals(1.5, sierFractal.getCanvas().getMaxCoords().getX(), 0.01);
    assertEquals(1.5, sierFractal.getCanvas().getMaxCoords().getY(), 0.01);
  }

  @Test
  void negative_testZoomOut() {
    double initialMinX = sierFractal.getCanvas().getMinCoords().getX();
    double initialMinY = sierFractal.getCanvas().getMinCoords().getY();
    double initialMaxX = sierFractal.getCanvas().getMaxCoords().getX();
    double initialMaxY = sierFractal.getCanvas().getMaxCoords().getY();

    sierFractal.zoomOut();

    assertNotEquals(initialMinX, sierFractal.getCanvas().getMinCoords().getX());
    assertNotEquals(initialMinY, sierFractal.getCanvas().getMinCoords().getY());
    assertNotEquals(initialMaxX, sierFractal.getCanvas().getMaxCoords().getX());
    assertNotEquals(initialMaxY, sierFractal.getCanvas().getMaxCoords().getY());
  }

  @Test
  void zoomInAndOut() {
    double initialMinX = sierFractal.getCanvas().getMinCoords().getX();
    double initialMinY = sierFractal.getCanvas().getMinCoords().getY();
    double initialMaxX = sierFractal.getCanvas().getMaxCoords().getX();
    double initialMaxY = sierFractal.getCanvas().getMaxCoords().getY();

    sierFractal.zoomIn();
    sierFractal.zoomOut();

    assertEquals(initialMinX, sierFractal.getCanvas().getMinCoords().getX(), 0.01);
    assertEquals(initialMinY, sierFractal.getCanvas().getMinCoords().getY(), 0.01);
    assertEquals(initialMaxX, sierFractal.getCanvas().getMaxCoords().getX(), 0.01);
    assertEquals(initialMaxY, sierFractal.getCanvas().getMaxCoords().getY(), 0.01);
  }

  @Test
  void negative_testZoomInAndOut() {
    sierFractal.zoomIn();
    sierFractal.zoomOut();

    assertNotEquals(0.5, sierFractal.getCanvas().getMinCoords().getX());
    assertNotEquals(0.5, sierFractal.getCanvas().getMinCoords().getY());
    assertNotEquals(0.5, sierFractal.getCanvas().getMaxCoords().getX());
    assertNotEquals(0.5, sierFractal.getCanvas().getMaxCoords().getY());
  }

  @Test
  void testResetView() {
    double initialMinX = sierFractal.getCanvas().getMinCoords().getX();
    double initialMinY = sierFractal.getCanvas().getMinCoords().getY();
    double initialMaxX = sierFractal.getCanvas().getMaxCoords().getX();
    double initialMaxY = sierFractal.getCanvas().getMaxCoords().getY();
    sierFractal.zoomIn();
    sierFractal.moveView(0.3, 0.9);

    sierFractal.resetView();

    assertEquals(initialMinX, sierFractal.getCanvas().getMinCoords().getX());
    assertEquals(initialMinY, sierFractal.getCanvas().getMinCoords().getY());
    assertEquals(initialMaxX, sierFractal.getCanvas().getMaxCoords().getX());
    assertEquals(initialMaxY, sierFractal.getCanvas().getMaxCoords().getY());
  }

  @Test
  void negative_testResetView() {
    sierFractal.zoomIn();
    sierFractal.moveView(0.3, 0.9);

    sierFractal.resetView();

    assertNotEquals(0.5, sierFractal.getCanvas().getMinCoords().getX());
    assertNotEquals(0.5, sierFractal.getCanvas().getMinCoords().getY());
    assertNotEquals(0.5, sierFractal.getCanvas().getMaxCoords().getX());
    assertNotEquals(0.5, sierFractal.getCanvas().getMaxCoords().getY());
  }

  @Test
  void testSetSteps() {
    int steps = 10000;
    sierFractal.setSteps(steps);
    assertEquals(steps, sierFractal.getSteps());
  }

  @Test
  void negative_testSetSteps() {
    int steps = 10000;
    sierFractal.setSteps(steps);
    assertNotEquals(1000, sierFractal.getSteps());
  }

  @Test
  void setDescription() {
    double juliMinX = juliFractal.getCanvas().getMinCoords().getX();
    double juliMinY = juliFractal.getCanvas().getMinCoords().getY();
    double juliMaxX = juliFractal.getCanvas().getMaxCoords().getX();
    double juliMaxY = juliFractal.getCanvas().getMaxCoords().getY();

    sierFractal.setDescription(juliDescription);
    sierFractal.resetView();

    assertEquals(juliMinX, sierFractal.getCanvas().getMinCoords().getX());
    assertEquals(juliMinY, sierFractal.getCanvas().getMinCoords().getY());
    assertEquals(juliMaxX, sierFractal.getCanvas().getMaxCoords().getX());
    assertEquals(juliMaxY, sierFractal.getCanvas().getMaxCoords().getY());
  }

  @Test
  void negative_testSetDescription() {
    double initialMinX = sierFractal.getCanvas().getMinCoords().getX();
    double initialMinY = sierFractal.getCanvas().getMinCoords().getY();
    double initialMaxX = sierFractal.getCanvas().getMaxCoords().getX();
    double initialMaxY = sierFractal.getCanvas().getMaxCoords().getY();

    sierFractal.setDescription(juliDescription);
    sierFractal.resetView();

    assertNotEquals(initialMinX, sierFractal.getCanvas().getMinCoords().getX());
    assertNotEquals(initialMinY, sierFractal.getCanvas().getMinCoords().getY());
    assertNotEquals(initialMaxX, sierFractal.getCanvas().getMaxCoords().getX());
    assertNotEquals(initialMaxY, sierFractal.getCanvas().getMaxCoords().getY());
  }

  @Test
  void testSetColorIteration() {
    sierFractal.setColorIteration(true);

    assertTrue(sierFractal.getColorIteration());
  }

  @Test
  void negative_testSetColorIteration() {
    sierFractal.setColorIteration(true);

    assertNotEquals(false, sierFractal.getColorIteration());
  }

  @Test
  void negative_testGetWritableImage() {
    sierFractal.createAffineImage(2000);

    assertNotNull(sierFractal.getWritableImage());
  }
}