package edu.ntnu.stud.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

class FractalFactoryTest {
  private Fractal fractal;

  @BeforeEach
  void setUp() {
    FractalFactory.initializeDescriptions();
    fractal = new Fractal();
  }

  @AfterEach
  void tearDown() {
    fractal = null;
  }

  @Test
  void testAvailableFractals() {
    FractalFactory.createSierpinski(fractal);
    String sierType = fractal.getDescription().getTransformationType();

    FractalFactory.createBarnsley(fractal);
    String barnType = fractal.getDescription().getTransformationType();

    FractalFactory.createJulia(fractal, "juliaSet2");
    String juliType = fractal.getDescription().getTransformationType();

    FractalFactory.createMandelbrot(fractal);
    String mandType = fractal.getDescription().getTransformationType();

    assertEquals("Affine2D", sierType);
    assertEquals("Affine2D", barnType);
    assertEquals("Julia", juliType);
    assertEquals("Mandel", mandType);
  }
}