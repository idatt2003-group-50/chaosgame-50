package edu.ntnu.stud.controller;

import java.util.List;

import javafx.embed.swing.JFXPanel;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable;
import static org.junit.jupiter.api.Assertions.*;

class CustomInputTest {

  @DisabledIfEnvironmentVariable(named = "CI", matches = "true", disabledReason = "Can't run GUI tests on CI server")
  @Test
  void testCustomAffineInput() {
    new JFXPanel(); // JFXPanel to initialize JavaFX Toolkit

    String transformationType = "Affine2D";
    List<TextField> descriptionCoordinates = List.of(
        new TextField("0.0"), new TextField("0.0"), new TextField("1.0"), new TextField("1.0"));
    TextArea affiInputText = new TextArea("1.0, 2.0, 3.0, 4.0, 5.0, 6.0\n7.0, 8.0, 9.0, 10.0, 11.0, 12.0");

    String affineInput = CustomInput.affineInput(transformationType, descriptionCoordinates, affiInputText);

    String expected = "Affine2D\n0.0,0.0\n1.0,1.0\n1.0, 2.0, 3.0, 4.0, 5.0, 6.0\n7.0, 8.0, 9.0, 10.0, 11.0, 12.0";
    assertEquals(expected, affineInput);
  }

  @DisabledIfEnvironmentVariable(named = "CI", matches = "true", disabledReason = "Can't run GUI tests on CI server")
  @Test
  void negative_testCustomAffineInput() {
    new JFXPanel(); // JFXPanel to initialize JavaFX Toolkit

    String transformationType = "Affine2D";
    List<TextField> descriptionCoordinates = List.of(
        new TextField("0.0"), new TextField("0.0"), new TextField("1.0"), new TextField("1.0"));
    TextArea affiInputText = new TextArea("1.0, 2.0, 3.0, 4.0, 5.0, 6.0\n7.0, 8.0, 9.0, 10.0, 11.0, 12.0");

    String affineInput = CustomInput.affineInput(transformationType, descriptionCoordinates, affiInputText);
    String unexpected = "Affine2D\n0.0,0.0\n1.0,1.0\n1.0, 2.0, 3.0, 4.0, 5.0, 6.0\n7.0, 8.0, 9.0, 10.0, 11.0, 13.0";

    assertNotEquals(unexpected, affineInput);
  }

  @DisabledIfEnvironmentVariable(named = "CI", matches = "true", disabledReason = "Can't run GUI tests on CI server")
  @Test
  void testCustomJuliaInput() {
    new JFXPanel(); // JFXPanel to initialize JavaFX Toolkit
    String transformationType = "Julia";
    List<TextField> descriptionCoordinates = List.of(
        new TextField("0.0"), new TextField("0.0"), new TextField("1.0"), new TextField("1.0"));
    List<TextField> juliaVectorFields = List.of(new TextField("1.0"), new TextField("2.0"));

    String juliaInput = CustomInput.juliaInput(transformationType, descriptionCoordinates, juliaVectorFields);
    String expected = "Julia\n0.0,0.0\n1.0,1.0\n1.0,2.0";

    assertEquals(expected, juliaInput);
  }

  @DisabledIfEnvironmentVariable(named = "CI", matches = "true", disabledReason = "Can't run GUI tests on CI server")
  @Test
  void negative_testCustomJuliaInput() {
    new JFXPanel(); // JFXPanel to initialize JavaFX Toolkit
    String transformationType = "Julia";
    List<TextField> descriptionCoordinates = List.of(
        new TextField("0.0"), new TextField("0.0"), new TextField("1.0"), new TextField("1.0"));
    List<TextField> juliaVectorFields = List.of(new TextField("1.0"), new TextField("2.0"));

    String juliaInput = CustomInput.juliaInput(transformationType, descriptionCoordinates, juliaVectorFields);
    String unexpected = "Julia\n0.0,0.0\n1.0,1.0\n1.0,3.0";

    assertNotEquals(unexpected, juliaInput);
  }
}

