package edu.ntnu.stud.controller;

import edu.ntnu.stud.exception.DescriptionException;
import edu.ntnu.stud.model.*;
import edu.ntnu.stud.model.linalg.AffineTransform2D;
import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Transform2D;
import edu.ntnu.stud.model.linalg.Vector2D;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class FileHandlerTest {

  @Test
  void testReadToDescription() {
    String filePath = "src/test/java/edu/ntnu/stud/resources/sierTest.txt";

    Matrix2x2 a = new Matrix2x2(0.5, 0, 0, 0.5);
    Vector2D b1 = new Vector2D(0, 0);
    Vector2D b2 = new Vector2D(0.25, 0.5);
    Vector2D b3 = new Vector2D(0.5, 0);

    AffineTransform2D t1 = new AffineTransform2D(a, b1);
    AffineTransform2D t2 = new AffineTransform2D(a, b2);
    AffineTransform2D t3 = new AffineTransform2D(a, b3);

    List<Transform2D> expectedTransformations = new ArrayList<>();
    expectedTransformations.add(t1);
    expectedTransformations.add(t2);
    expectedTransformations.add(t3);

    try {
      Description description = FileHandler.readToDescription(filePath);

      // Check if the contents of the lists are equal
      for (int i = 0; i < expectedTransformations.size(); i++) {
        assertEquals(expectedTransformations.get(i).toString(), description.getTransformations().get(i).toString());
      }
      assertEquals(expectedTransformations.size(), description.getTransformations().size());
    } catch (DescriptionException e) {
      fail(e.getMessage());
    }
  }

  @Test
  void negative_testReadDescription() {
    String filePath = "src/test/java/edu/ntnu/stud/resources/sierTest.txt";

    Matrix2x2 a = new Matrix2x2(9, 9, 9, 9);
    Vector2D b1 = new Vector2D(9, 9);
    Vector2D b2 = new Vector2D(9, 9);

    AffineTransform2D t1 = new AffineTransform2D(a, b1);
    AffineTransform2D t2 = new AffineTransform2D(a, b2);

    List<Transform2D> unexpectedTransformations = new ArrayList<>();
    unexpectedTransformations.add(t1);
    unexpectedTransformations.add(t2);

    try {
      Description description = FileHandler.readToDescription(filePath);

      // Check if the contents of the lists are equal
      for (int i = 0; i < unexpectedTransformations.size(); i++) {
        assertNotEquals(unexpectedTransformations.get(i).toString(), description.getTransformations().get(i).toString());
      }
      assertNotEquals(unexpectedTransformations.size(), description.getTransformations().size());
    } catch (DescriptionException e) {
      fail(e.getMessage());
    }
  }

  @Test
  void testDescriptionException_invalidDescription() {
    String filePath = "src/test/java/edu/ntnu/stud/resources/invalidDescription.txt";
    assertThrows(DescriptionException.class, () -> FileHandler.readToDescription(filePath));
  }

  @Test
  void testDescriptionException_invalidTransType() {
    String filePath = "src/test/java/edu/ntnu/stud/resources/invalidTransType.txt";
    try {
      FileHandler.readToDescription(filePath);
    } catch (DescriptionException e) {
      assertEquals("Invalid transformation type", e.getMessage());
    }
  }

  @Test
  void testDescriptionException_invalidMinCoords() {
    String filePath = "src/test/java/edu/ntnu/stud/resources/invalidMinCoords.txt";
    try {
      FileHandler.readToDescription(filePath);
    } catch (DescriptionException e) {
      assertEquals("Invalid input in minimum coordinates", e.getMessage());
    }
  }

  @Test
  void testDescriptionException_invalidMaxCoords() {
    String filePath = "src/test/java/edu/ntnu/stud/resources/invalidMaxCoords.txt";
    try {
      FileHandler.readToDescription(filePath);
    } catch (DescriptionException e) {
      assertEquals("Invalid input in maximum coordinates", e.getMessage());
    }
  }

  @Test
  void testDescriptionException_invalidAffineParams() {
    String filePath = "src/test/java/edu/ntnu/stud/resources/invalidAffineParams.txt";
    try {
      FileHandler.readToDescription(filePath);
    } catch (DescriptionException e) {
      assertEquals("Invalid input in Affine Transformation (line: 1)", e.getMessage());
    }
  }

  @Test
  void testDescriptionException_invalidJuliaParams() {
    String filePath = "src/test/java/edu/ntnu/stud/resources/invalidJuliaParams.txt";
    try {
      FileHandler.readToDescription(filePath);
    } catch (DescriptionException e) {
      assertEquals("Invalid input in Julia Transformation (c)", e.getMessage());
    }
  }

  @Test
  void testParseVector2D() {
    String vectorString = "1.0, 2.0";
    Vector2D expectedVector = new Vector2D(1.0, 2.0);
    double expectedX = expectedVector.getX();
    double expectedY = expectedVector.getY();

    Vector2D parsedVector = FileHandler.parseVector2D(vectorString);
    double parsedX = parsedVector.getX();
    double parsedY = parsedVector.getY();

    assertEquals(expectedX, parsedX);
    assertEquals(expectedY, parsedY);
  }

  @Test
  void negative_testParseVector2D() {
    String vectorString = "1.0, 2.0";
    Vector2D unexpectedVector = new Vector2D(2.0, 1.0);
    double unexpectedX = unexpectedVector.getX();
    double unexpectedY = unexpectedVector.getY();

    Vector2D parsedVector = FileHandler.parseVector2D(vectorString);
    double parsedX = parsedVector.getX();
    double parsedY = parsedVector.getY();

    assertNotEquals(unexpectedX, parsedX);
    assertNotEquals(unexpectedY, parsedY);
  }

  @Test
  void testParseVector2D_invalidFormat() {
    String vectorString = "1.0, 2.0, 3.0";
    Vector2D parsedVector = FileHandler.parseVector2D(vectorString);
    assertNull(parsedVector);
  }

  @Test
  void testParseVector2D_invalidNumericFormat() {
    String vectorString = "1.0, a";
    Vector2D parsedVector = FileHandler.parseVector2D(vectorString);
    assertNull(parsedVector);
  }

  @Test
  void testParseAffineParams() {
    String affineString = "1.0, 2.0, 3.0, 4.0, 5.0, 6.0";
    double[] expectedParams = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};

    double[] parsedParams = FileHandler.parseAffineParams(affineString);

    for (int i = 0; i < expectedParams.length; i++) {
      assertNotNull(parsedParams);
      assertEquals(expectedParams[i], parsedParams[i]);
    }
  }

  @Test
  void negative_testParseAffineParams() {
    String affineString = "1.0, 2.0, 3.0, 4.0, 5.0, 6.0";
    double[] unexpectedParams = {9.0, 9.0, 9.0, 9.0, 9.0, 9.0};

    double[] parsedParams = FileHandler.parseAffineParams(affineString);

    for (int i = 0; i < unexpectedParams.length; i++) {
      assertNotNull(parsedParams);
      assertNotEquals(unexpectedParams[i], parsedParams[i]);
    }
  }

  @Test
  void testParseAffineParams_invalidFormat() {
    String affineString = "1.0, 2.0, 3.0, 4.0, 5.0";
    double[] parsedParams = FileHandler.parseAffineParams(affineString);
    assertNull(parsedParams);
  }

  @Test
  void testParseAffineParams_invalidNumericFormat() {
    String affineString = "1.0, 2.0, 3.0, 4.0, a, 6.0";
    double[] parsedParams = FileHandler.parseAffineParams(affineString);
    assertNull(parsedParams);
  }

  @Test
  void testParseJuliaParams() {
    String juliaString = "1.0, 2.0";
    double[] expectedParams = {1.0, 2.0};

    double[] parsedParams = FileHandler.parseJuliaParams(juliaString);

    for (int i = 0; i < expectedParams.length; i++) {
      assertNotNull(parsedParams);
      assertEquals(expectedParams[i], parsedParams[i]);
    }
  }

  @Test
  void negative_testParseJuliaParams() {
    String juliaString = "1.0, 2.0";
    double[] unexpectedParams = {9.0, 9.0};

    double[] parsedParams = FileHandler.parseJuliaParams(juliaString);

    for (int i = 0; i < unexpectedParams.length; i++) {
      assertNotNull(parsedParams);
      assertNotEquals(unexpectedParams[i], parsedParams[i]);
    }
  }

  @Test
  void testParseJuliaParams_invalidFormat() {
    String juliaString = "1.0, 2.0, 3.0";
    double[] parsedParams = FileHandler.parseJuliaParams(juliaString);
    assertNull(parsedParams);
  }

  @Test
  void testParseJuliaParams_invalidNumericFormat() {
    String juliaString = "1.0, a";
    double[] parsedParams = FileHandler.parseJuliaParams(juliaString);
    assertNull(parsedParams);
  }
}