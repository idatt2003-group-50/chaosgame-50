package edu.ntnu.stud;

import edu.ntnu.stud.view.AppLauncher;

/**
 * <STRONG>Chaos Game</STRONG><br>
 *
 * <p>JavaFX application that displays and manipulates fractals.
 *
 *<p>@author Hans Lawrence Pettersen and Daniel Moe Kvarsnes.<br>
 *
 *@version 5.0
 *</p>
 */
public class Main {
  /**
   * The main entry point for the application.
   * Calls {@link AppLauncher#launcher(String[])} which launches the JavaFX application.
   */
  public static void main(String[] args) {
    AppLauncher.launcher(args);
  }
}