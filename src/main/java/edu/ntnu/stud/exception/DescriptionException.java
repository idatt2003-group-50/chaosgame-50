package edu.ntnu.stud.exception;

/**
 * Exception for invalid <code>description</code> input.
 *
 * <p>Thrown by methods in:
 * <ul>
 *   <li> {@link edu.ntnu.stud.controller.FileHandler}
 *   <li> {@link edu.ntnu.stud.model.FractalFactory}
 * </ul>
 * </p>
 */
public class DescriptionException extends Exception {
  public DescriptionException(String message) {
    super(message);
  }
}
