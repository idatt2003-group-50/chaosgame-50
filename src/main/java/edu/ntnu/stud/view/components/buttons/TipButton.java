package edu.ntnu.stud.view.components.buttons;

import javafx.animation.PauseTransition;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

/**
 * Circular button with a tooltip that appears when the mouse hovers over it.
 */
public class TipButton extends Button {
  private final Tooltip customTooltip;
  private int size;

  /**
   * Constructs a circular button with a tooltip.
   *
   * @param buttonText Text displayed on the button.
   * @param tipText    Text displayed in the tooltip.
   */
  public TipButton(String buttonText, String tipText) {
    super(buttonText);

    size = 35;

    // Circle button shape
    Circle circle = new Circle((double) size / 2);
    circle.setFill(Color.web("#6e2e93"));
    setShape(circle);
    getStyleClass().add("tip-button");

    // Button settings
    setMaxSize(size, size);
    setMinSize(size, size);

    // Tooltip settings
    StackPane tipPane = new StackPane();
    tipPane.setStyle("-fx-text-fill: #ffffff; -fx-font-size: 14px;"
        + " -fx-padding: 5px; -fx-background-radius: 10;");
    Label tipTextArea = new Label(tipText);
    tipTextArea.setAlignment(Pos.TOP_CENTER);
    tipTextArea.setWrapText(true);
    tipPane.getChildren().add(tipTextArea);

    // Tooltip show/hide settings
    customTooltip = new Tooltip();
    customTooltip.setGraphic(tipPane);
    customTooltip.setPrefWidth(200);
    customTooltip.setPrefHeight(50);

    PauseTransition pause = new PauseTransition(Duration.seconds(2));
    pause.setOnFinished(e -> customTooltip.hide());

    setOnMouseClicked(event -> {
      Point2D point = localToScreen((getWidth() / 2) + 25, getHeight() / 2);
      customTooltip.show(this, point.getX(), point.getY());
    });

    setOnMouseExited(event -> customTooltip.hide());
  }

  /**
   * Set the size of the tooltip button.
   *
   * @param width  Width of the tooltip area.
   * @param height Height of the tooltip area.
   */
  public void setTooltipArea(int width, int height) {
    customTooltip.setPrefWidth(width);
    customTooltip.setPrefHeight(height);
  }

  /**
   * Set the size of the circular button.
   *
   * @param size Size of the circular button.
   */
  public void setButtonSize(int size) {
    this.size = size;
    setMaxSize(size, size);
    setMinSize(size, size);
  }
}