package edu.ntnu.stud.view.components;

import static edu.ntnu.stud.model.Fractal.DEFAULT_ZOOM_FACTOR;
import static edu.ntnu.stud.model.FractalFactory.DEFAULT_AFFINE_ITERATION;
import static edu.ntnu.stud.model.FractalFactory.DEFAULT_JULIA_ITERATION;

import edu.ntnu.stud.view.components.buttons.TipButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * Lower banner for the application window. Features modes for fractal types.
 */
public class Footer extends HBox {
  private boolean toggleColor;
  private Slider affineSlider;
  private Slider juliaSlider;
  private Button zoomInButton;
  private Button zoomOutButton;
  private ComboBox<String> zoomFactor;
  private ComboBox<String> juliaSetMenu;
  private ComboBox<String> colorMenu;
  private ToggleButton toggleButton;
  private final TipButton startTip;
  private final VBox affiSliderBox;
  private final VBox juliSliderBox;
  private final VBox juliSetBox;
  private final VBox colorBox;
  private final VBox affiColBox;
  private final HBox zoomBox;
  private final HBox tipBox;

  /**
   * Constructor for the footer of the application window.
   * Initializes the various components of the footer.
   */
  public Footer() {
    super();
    setStyle("-fx-background-color: #dcdcdc;"
        + "-fx-padding: 5px;");

    // Apply drop shadow effect
    DropShadow shadow = new DropShadow();
    setEffect(shadow);

    // Initialize components
    affiSliderBox = createAffineSlider();
    juliSliderBox = createJuliaSlider();
    juliSetBox = createJuliaSetBox();
    affiColBox = createToggleColorBox();
    colorBox = createColorBox();
    zoomBox = createZoomBox();
    tipBox = createTooltip();

    // Startup tip
    String tipText = "These buttons show tips on how to use the app."
        + "\n\nSelect a fractal in the header to begin!";
    startTip = new TipButton("?", tipText);
    startTip.setTooltipArea(310, 130);

    configureLayout();
  }

  private void configureLayout() {
    setAlignment(Pos.CENTER);

    setHgrow(affineSlider, Priority.ALWAYS);
    setHgrow(juliaSlider, Priority.ALWAYS);

    setPadding(new Insets(0, 25, 0, 25)); // Set padding on left and right sides
    setSpacing(50);
  }

  /**
   * Set the footer to the start mode, showing only a tooltip.
   * This mode is displayed when the application is started.
   */
  public void startMode() {
    getChildren().clear();
    getChildren().addAll(startTip);
    configureLayout();
  }

  /**
   * Set the footer to affine mode, showing:
   * <ul>
   *   <li>Toggle button for color iterations</li>
   *   <li>Slider for affine iterations</li>
   *   <li>Tooltip button</li>
   *   <li>Zoom buttons</li>
   *   <li>Zoom factor selection</li>
   * </ul>
   */
  public void affineMode() {
    getChildren().clear();
    getChildren().addAll(affiColBox, affiSliderBox, tipBox,  zoomBox);
    configureLayout();
  }

  /**
   * Set the footer to Julia mode, showing:
   * <ul>
   *   <li>Julia set selection</li>
   *   <li>Slider for Julia iterations</li>
   *   <li>Tooltip button</li>
   *   <li>Zoom buttons</li>
   *   <li>Zoom factor selection</li>
   * </ul>
   */
  public void juliaMode() {
    getChildren().clear();
    getChildren().addAll(juliSetBox, juliSliderBox, tipBox, zoomBox);
    configureLayout();
  }

  /**
   * Set the footer to Mandelbrot mode, showing:
   * <ul>
   *   <li>Slider for Julia iterations</li>
   *   <li>Tooltip button</li>
   *   <li>Zoom buttons</li>
   *   <li>Zoom factor selection</li>
   * </ul>
   */
  public void mandelMode() {
    getChildren().clear();
    getChildren().addAll(colorBox, juliSliderBox, tipBox, zoomBox);
    configureLayout();
  }

  private VBox createAffineSlider() {
    affineSlider = new Slider(0, 1000000, DEFAULT_AFFINE_ITERATION);
    affineSlider.setMajorTickUnit(250000);
    affineSlider.setShowTickMarks(true);
    affineSlider.setShowTickLabels(true);

    VBox sliderBox = new VBox();
    sliderBox.setAlignment(Pos.CENTER);
    sliderBox.setSpacing(10);
    sliderBox.setPrefWidth(400);
    Label sliderLabel = new Label("Iterations");
    sliderBox.getChildren().addAll(sliderLabel, affineSlider);

    return sliderBox;
  }

  private VBox createJuliaSlider() {
    juliaSlider = new Slider(0, 2000, DEFAULT_JULIA_ITERATION);
    juliaSlider.setMajorTickUnit(500);
    juliaSlider.setShowTickMarks(true);
    juliaSlider.setShowTickLabels(true);

    VBox sliderBox = new VBox(5);
    sliderBox.setAlignment(Pos.CENTER);
    sliderBox.setSpacing(10);
    sliderBox.setPrefWidth(400);
    Label sliderLabel = new Label("Iterations");
    sliderBox.getChildren().addAll(sliderLabel, juliaSlider);

    return sliderBox;
  }

  private VBox createJuliaSetBox() {
    juliaSetMenu = new ComboBox<>();
    juliaSetMenu.setPrefSize(150, 20);
    juliaSetMenu.setStyle("-fx-font-size: 13px; -fx-padding: 5px 0px;");
    juliaSetMenu.setValue("- 0.835 - 0.2321i"); //juliaSet2.txt default value

    String[] allJuliaSets = new String[]{
        "- 0.7269 + 0.1889i", "- 0.835 - 0.2321i", "- 0.285 + 0.01i", "- 0.4 + 0.6i"};
    juliaSetMenu.getItems().addAll(allJuliaSets);

    VBox juliaBox = new VBox(8);
    juliaBox.setAlignment(Pos.CENTER);
    Label juliaLabel = new Label("Julia Sets");
    juliaBox.getChildren().addAll(juliaLabel, juliaSetMenu);

    return juliaBox;
  }

  private VBox createColorBox() {
    colorMenu = new ComboBox<>();
    colorMenu.setPrefSize(150, 20);
    colorMenu.setStyle("-fx-font-size: 13px; -fx-padding: 5px 0px;");
    colorMenu.setValue("Ether");

    String[] allColors = new String[]{
        "Static", "Apollo", "B&W", "Ether"};
    colorMenu.getItems().addAll(allColors);

    VBox colorBox = new VBox(8);
    colorBox.setAlignment(Pos.CENTER);
    Label colorLabel = new Label("Color Scheme");
    colorBox.getChildren().addAll(colorLabel, colorMenu);

    return colorBox;
  }

  private VBox createToggleColorBox() {
    toggleButton = new ToggleButton();
    toggleButton.setMinWidth(50);
    toggleButton.setText("OFF");

    toggleButton.setOnAction(event -> {
      if (toggleButton.isSelected()) {
        toggleButton.setText("ON");
        toggleButton.setStyle("-fx-background-color: #27ae60; -fx-text-fill: #ffffff;");
        toggleColor = true;
      } else {
        toggleButton.setText("OFF");
        toggleButton.setStyle("-fx-background-color: #9e2e2e; -fx-text-fill: #ffffff;");
        toggleColor = false;
      }
    });

    VBox toggleBox = new VBox();
    toggleBox.setAlignment(Pos.CENTER);
    toggleBox.setSpacing(10);
    toggleBox.setPrefWidth(150);
    Label toggleLabel = new Label("Color Iteration");
    toggleBox.getChildren().addAll(toggleLabel, toggleButton);

    return toggleBox;
  }

  private HBox createZoomBox() {
    zoomInButton = new Button("+");
    zoomOutButton = new Button("-");
    Button[] zoomButtons = new Button[]{zoomInButton, zoomOutButton};
    int butSize = 30;

    for (Button button : zoomButtons) {
      button.setMinWidth(butSize);
      button.setMinHeight(butSize);
      button.getStyleClass().add("zoom-button");
      button.setAlignment(Pos.CENTER);
    }

    // Double to formatted string conversion
    String defaultZoom = String.format("%.0f%%", DEFAULT_ZOOM_FACTOR * 100);

    // Create menu items for zoom options
    ObservableList<String> zoomOptions = FXCollections.observableArrayList(
        "500%", "300%", "200%", defaultZoom
    );
    zoomFactor = new ComboBox<>(zoomOptions);
    zoomFactor.setValue(defaultZoom); // Default zoom factor
    zoomFactor.setPrefWidth(75);
    zoomFactor.setStyle("-fx-font-size: 12px; -fx-padding: 2px 0px;");

    HBox zoomRow1 = new HBox();
    zoomRow1.setAlignment(Pos.CENTER);
    zoomRow1.setSpacing(10);
    zoomRow1.getChildren().addAll(zoomFactor);

    HBox zoomRow2 = new HBox();
    zoomRow2.setAlignment(Pos.CENTER);
    zoomRow2.setSpacing(15);
    zoomRow2.getChildren().addAll(zoomOutButton, zoomInButton);

    // Stack rows vertically
    VBox zoomRows = new VBox(10);
    zoomRows.setAlignment(Pos.CENTER);
    zoomRows.getChildren().addAll(zoomRow2, zoomRow1);

    Label zoomCalcLabel = new Label("Calc. Zoom");
    Label zoomFactorLabel = new Label("Zoom Factor");

    // VBox for zoom labels
    VBox zoomLabelBox = new VBox(25);
    zoomLabelBox.setAlignment(Pos.CENTER_LEFT);
    zoomLabelBox.getChildren().addAll(zoomCalcLabel, zoomFactorLabel);

    // HBox for zoom buttons and labels
    HBox gatheredZoomBox = new HBox();
    gatheredZoomBox.setAlignment(Pos.CENTER);
    gatheredZoomBox.setPrefWidth(225);
    gatheredZoomBox.setSpacing(10);
    gatheredZoomBox.getChildren().addAll(zoomRows, zoomLabelBox);

    return gatheredZoomBox;
  }

  private HBox createTooltip() {
    // Tooltip info
    String tipText = "Left-click anywhere on the image to center on that point."
        + "\n\nRight click anywhere on the image to reset the view.";
    TipButton tipButton = new TipButton("?", tipText);
    tipButton.setTooltipArea(235, 150);
    HBox tipbox = new HBox();
    tipbox.setAlignment(Pos.CENTER);
    tipbox.setMaxSize(30, 30);
    tipbox.getChildren().add(tipButton);
    return tipbox;
  }

  public void resetSlider() {
    affineSlider.setValue(DEFAULT_AFFINE_ITERATION);
    juliaSlider.setValue(DEFAULT_JULIA_ITERATION);
  }

  public ComboBox<String> getJuliaSetMenu() {
    return juliaSetMenu;
  }

  public ComboBox<String> getZoomFactor() {
    return zoomFactor;
  }

  public Button getZoomInButton() {
    return zoomInButton;
  }

  public Button getZoomOutButton() {
    return zoomOutButton;
  }

  public ToggleButton getToggleButton() {
    return toggleButton;
  }

  public Slider getAffineSlider() {
    return affineSlider;
  }

  public Slider getJuliaSlider() {
    return juliaSlider;
  }

  public ComboBox<String> getColorMenu() {
    return colorMenu;
  }
}