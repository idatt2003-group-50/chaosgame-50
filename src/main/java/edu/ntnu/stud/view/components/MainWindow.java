package edu.ntnu.stud.view.components;

import edu.ntnu.stud.exception.DescriptionException;
import edu.ntnu.stud.model.Fractal;
import edu.ntnu.stud.model.FractalFactory;
import edu.ntnu.stud.model.FractalObserver;
import javafx.application.Platform;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

/**
 * Main window of the application. Contains a fractal display area, header and footer.
 * <p> Implements {@link FractalObserver} and {@link OverlayObserver}.</p>
 *
 * @see #update()
 * @see #customFractal()
 */
public class MainWindow implements FractalObserver, OverlayObserver {
  private final Fractal fractal;
  private final Overlay overlay;
  private final GridPane window;
  private final Header header;
  private final Footer footer;
  private final ImageView imageView;
  private boolean toggleColor = false;
  private static final double DEFAULT_HEIGHT = 950;
  private static final String DEFAULT_PATH = "juliaSet2";
  private String juliaSetName = DEFAULT_PATH;

  /**
   * Constructor for the main window of the application.
   * Initializes the various components of the window.
   *
   * @param fractal fractal to be displayed
   * @param overlay overlay for creating custom fractals
   */
  public MainWindow(Fractal fractal, Overlay overlay) {
    this.fractal = fractal;
    this.overlay = overlay;
    window = new GridPane();
    header = new Header();
    footer = new Footer();
    footer.startMode();

    // Set preferred size of the window
    double width = DEFAULT_HEIGHT * 0.9;
    window.setPrefSize(width, DEFAULT_HEIGHT);

    // Set background color of the frame
    window.setStyle("-fx-background-color: #000000;");

    // Row constraints
    RowConstraints headerRow = new RowConstraints();
    RowConstraints displayRow = new RowConstraints();
    RowConstraints footerRow = new RowConstraints();
    headerRow.setPercentHeight(10); // header height (row index 0)
    displayRow.setPercentHeight(80); // display height (row index 1)
    footerRow.setPercentHeight(10); // footer height (row index 2)
    window.getRowConstraints().addAll(headerRow, displayRow, footerRow);

    // Column constraints
    ColumnConstraints onlyCol = new ColumnConstraints();
    onlyCol.setPercentWidth(100); // (column index 0)
    window.getColumnConstraints().add(onlyCol);

    // Add header and footer to the window
    window.add(header, 0, 0);
    window.add(footer, 0, 2);

    // Fractal display area
    imageView = new ImageView();
    imageView.setPreserveRatio(true);
    ReadOnlyDoubleProperty dynamicHeight = window.heightProperty();
    DoubleBinding desiredHeight = dynamicHeight.multiply(0.8);
    DoubleBinding desiredWidth = dynamicHeight.multiply(0.8);
    imageView.fitHeightProperty().bind(desiredHeight);
    imageView.fitWidthProperty().bind(desiredWidth);

    window.add(imageView, 0, 1); // Add imageView to the window
    GridPane.setHalignment(imageView, HPos.CENTER);
    GridPane.setValignment(imageView, VPos.CENTER);

    // Set actions
    setMouseActions();
    setHeaderActions();
    setFooterActions();
  }

  /**
   * Update the fractal display area with the latest image.
   *
   * @see edu.ntnu.stud.model.Fractal#getWritableImage()
   */
  @Override
  public void update() {
    imageView.setImage(fractal.getWritableImage());
  }

  /**
   * Creates a custom fractal based on the users input.
   * <p>
   *   Shows an error message if the input is invalid.
   * </p>
   *
   * @see edu.ntnu.stud.model.FractalFactory#createCustom(Fractal)
   */
  @Override
  public void customFractal() {
    switch (overlay.getTransType()) {
      case "Affine2D":
        footer.affineMode();
        break;
      case "Julia":
        footer.juliaMode();
        break;
      default:
    }
    try {
      FractalFactory.createCustom(fractal);
    } catch (DescriptionException ex) {
      displayErrorMessage(ex.getMessage());
    }
  }

  /**
   * Displays {@link edu.ntnu.stud.exception.DescriptionException} message in a pop-up window.
   *
   * @param message information about the invalid input
   */
  static void displayErrorMessage(String message) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Invalid Input");
    alert.setHeaderText(null);
    alert.setContentText(message);
    alert.showAndWait();
  }

  /**
   * Sets the actions for the mouse clicks on the fractal display area.
   * Normalizes the coordinates of the mouse click.
   *
   * <li>Left click:
   * {@link edu.ntnu.stud.model.Fractal#moveView(double, double)}</li>
   * <li>Right click: {@link edu.ntnu.stud.model.Fractal#resetView()}
   *
   */
  private void setMouseActions() {
    imageView.setOnMouseClicked(event -> {
      if (event.getButton() == MouseButton.PRIMARY) {
        // Get the coordinates of the mouse click
        double xMouse = event.getX();
        double yMouse = event.getY();

        // Normalize the coordinates
        double xNorm = xMouse / imageView.getFitWidth();
        double yNorm = yMouse / imageView.getFitHeight();

        // Recenter the canvas
        fractal.moveView(xNorm, yNorm);
      }
      if (event.getButton() == MouseButton.SECONDARY) {
        fractal.resetView();
      }
    });
  }

  private void setHeaderActions() {
    header.getExitButton().setOnAction(event -> Platform.exit());

    header.getSaveButton().setOnAction(event -> fractal.saveImage());

    header.getSierButton().setOnAction(event -> {
      footer.affineMode();
      footer.resetSlider();
      FractalFactory.createSierpinski(fractal);
    });

    header.getBarnButton().setOnAction(event -> {
      footer.affineMode();
      footer.resetSlider();
      FractalFactory.createBarnsley(fractal);
    });

    header.getJuliButton().setOnAction(event -> {
      footer.juliaMode();
      footer.resetSlider();
      FractalFactory.createJulia(fractal, juliaSetName);
    });

    header.getMandButton().setOnAction(event -> {
      footer.mandelMode();
      footer.resetSlider();
      FractalFactory.createMandelbrot(fractal);
    });

    header.getCustButton().setOnAction(event -> {
      footer.resetSlider();
      overlay.showWindow();
    });
  }

  private void setFooterActions() {
    footer.getToggleButton().selectedProperty().addListener((obs, oldValue, newValue) -> {
      toggleColor = newValue;
      fractal.setColorIteration(toggleColor);
    });

    footer.getJuliaSlider().valueProperty().addListener((obs, oldValue, newValue) ->
        fractal.setSteps((int) newValue.doubleValue()));

    footer.getAffineSlider().valueProperty().addListener((obs, oldValue, newValue) ->
        fractal.setSteps((int) newValue.doubleValue()));

    footer.getColorMenu().valueProperty().addListener((obs, oldValue, newValue) -> {
      if (newValue != null) {
        fractal.setMandelScheme(newValue);
      }
    });

    footer.getZoomInButton().setOnAction(event -> fractal.zoomIn());

    footer.getZoomOutButton().setOnAction(event -> fractal.zoomOut());

    footer.getZoomFactor().valueProperty().addListener((obs, oldValue, newValue) -> {
      if (newValue != null) {
        double zoomFactor = Double.parseDouble(newValue.replace("%", "")) / 100.0;
        fractal.setZoomFactor(zoomFactor);
      }
    });

    ComboBox<String> juliaMenu = footer.getJuliaSetMenu();
    juliaMenu.valueProperty().addListener((obs, oldValue, newValue) -> {
      if (newValue != null) {
        // Find the index of the selected item
        int index = juliaMenu.getItems().indexOf(newValue) + 1;
        juliaSetName = "juliaSet" + index;
        FractalFactory.createJulia(fractal, juliaSetName);
      }
    });
  }

  public GridPane getWindow() {
    return window;
  }
}