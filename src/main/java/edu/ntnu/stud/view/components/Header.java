package edu.ntnu.stud.view.components;

import edu.ntnu.stud.view.components.buttons.HeaderButton;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * Upper banner for the application window. Features buttons for fractal types, saving and exiting.
 */
public class Header extends HBox {
  private final Button sierButton;
  private final Button barnButton;
  private final Button juliButton;
  private final Button mandButton;
  private final Button custButton;
  private final Button saveButton;
  private final Button exitButton;

  /**
   * Constructor for the header of the application window.
   * Initializes the various components of the header.
   */
  public Header() {
    super();
    setStyle("-fx-background-color: #dcdcdc;"
        + "-fx-padding: 5px;");

    // Apply drop shadow effect
    DropShadow shadow = new DropShadow();
    setEffect(shadow);

    sierButton = new HeaderButton("Sierpinski Triangle");
    barnButton = new HeaderButton("Barnsley Fern");
    juliButton = new HeaderButton("Julia Set");
    mandButton = new HeaderButton("Mandelbrot Set");
    custButton = new Button("Custom Fractal");
    saveButton = new Button("Save Image");
    exitButton = new Button("Exit");

    custButton.getStyleClass().add("cust-button");
    saveButton.getStyleClass().add("save-button");
    exitButton.getStyleClass().add("exit-button");

    // Set max width and height of buttons
    Button[] buttons = {
        sierButton, barnButton, juliButton, mandButton, custButton, saveButton, exitButton};
    for (Button button : buttons) {
      button.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
      setHgrow(button, Priority.ALWAYS);
    }

    // Fractal buttons
    HBox setBox = new HBox();
    setBox.getChildren().addAll(sierButton, barnButton, juliButton, mandButton, custButton);
    setBox.setAlignment(Pos.CENTER_LEFT);
    setHgrow(setBox, Priority.ALWAYS);
    setBox.setSpacing(5);

    // Other buttons
    HBox otherBox = new HBox();
    otherBox.getChildren().addAll(saveButton, exitButton);
    otherBox.setAlignment(Pos.CENTER_RIGHT);
    otherBox.setSpacing(5);

    // Add button boxes to header
    setSpacing(15);
    getChildren().addAll(setBox, otherBox);
  }

  public Button getSierButton() {
    return sierButton;
  }

  public Button getBarnButton() {
    return barnButton;
  }

  public Button getJuliButton() {
    return juliButton;
  }

  public Button getMandButton() {
    return mandButton;
  }

  public Button getCustButton() {
    return custButton;
  }

  public Button getSaveButton() {
    return saveButton;
  }

  public Button getExitButton() {
    return exitButton;
  }
}