package edu.ntnu.stud.view.components.buttons;

import javafx.scene.control.Button;

/**
 * Button with a fractal theme.
 */
public class HeaderButton extends Button {
  public HeaderButton(String text) {
    super(text);
    getStyleClass().add("fractal-button");
  }
}