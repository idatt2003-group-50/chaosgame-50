package edu.ntnu.stud.view.components;

/**
 * Observer interface for <code>overlay</code> changes.
 * <p> Implemented by {@link edu.ntnu.stud.view.components.MainWindow}.</p>
 *
 * @see Overlay
 */
public interface OverlayObserver {
  void customFractal();
}
