package edu.ntnu.stud.view.components;

import static javafx.scene.layout.HBox.setHgrow;

import edu.ntnu.stud.controller.CustomInput;
import edu.ntnu.stud.controller.FileHandler;
import edu.ntnu.stud.exception.DescriptionException;
import edu.ntnu.stud.view.components.buttons.HeaderButton;
import edu.ntnu.stud.view.components.buttons.TipButton;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Overlay for creating custom fractals. Contains input fields for affine and Julia transformations.
 * Also contains buttons for loading and saving custom fractals.
 *
 * @see FileHandler
 * @see CustomInput
 * @see OverlayObserver
 * @see DescriptionException
 */
public class Overlay extends DialogPane {
  private Stage primaryStage;
  private Button affiButton;
  private Button juliButton;
  private Button genrButton;
  private Button loadButton;
  private Button saveButton;
  private VBox juliBox;
  private VBox affiBox;
  private VBox cordBox;
  private VBox centerBox;
  private String transType;
  private final String labelStyle = "-fx-font-size: 15px; ";
  private List<TextField> juliaVectorFields;
  private List<TextField> descriptionCoordinates;
  private TextArea affiInputText;
  private HBox affiInputBox;
  private List<String[]> fileAsString;
  private final List<OverlayObserver> observers = new ArrayList<>();

  /**
   * Constructor for the overlay of the application window.
   * Initializes the various components of the overlay.
   */
  public void showWindow() {
    primaryStage = new Stage();
    primaryStage.setTitle("Custom Fractal");
    primaryStage.setWidth(450);
    primaryStage.setHeight(650);

    DropShadow shadow = new DropShadow();
    setEffect(shadow);

    // Create a GridPane for layout
    GridPane window = new GridPane();

    // Define row constraints
    RowConstraints headerRow = new RowConstraints();
    RowConstraints displayRow = new RowConstraints();
    RowConstraints footerRow = new RowConstraints();
    headerRow.setPercentHeight(15); // header height (row index 0)
    displayRow.setPercentHeight(70); // display height (row index 1)
    footerRow.setPercentHeight(15); // footer height (row index 2)
    window.getRowConstraints().addAll(headerRow, displayRow, footerRow);

    // Define column constraints
    ColumnConstraints onlyCol = new ColumnConstraints();
    onlyCol.setPercentWidth(100); // (column index 0)
    window.getColumnConstraints().add(onlyCol);

    // Header row, transformation buttons
    affiButton = new HeaderButton("Affine Transform");
    juliButton = new HeaderButton("Julia Transform");
    loadButton = new HeaderButton("Load From File");
    loadButton.getStyleClass().add("load-button");

    // Set max width and height of buttons
    Button[] headBut = {affiButton, juliButton, loadButton};
    for (Button button : headBut) {
      button.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
      setHgrow(button, Priority.ALWAYS);
    }

    HBox headBox = new HBox();
    headBox.getChildren().addAll(affiButton, juliButton, loadButton);
    headBox.setSpacing(5);
    headBox.setAlignment(Pos.CENTER);
    setHgrow(headBox, Priority.ALWAYS);
    headBox.setStyle("-fx-background-color: #dcdcdc; -fx-padding: 5px;");

    window.add(headBox, 0, 0);

    // Footer row
    genrButton = new HeaderButton("Generate Fractal");
    saveButton = new HeaderButton("Save as .txt file");
    saveButton.getStyleClass().add("save-button");
    genrButton.getStyleClass().add("cust-button");

    // Set max width and height of buttons
    Button[] footBut = {genrButton, saveButton};
    for (Button button : footBut) {
      button.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
      setHgrow(button, Priority.ALWAYS);
    }

    HBox footBox = new HBox();
    footBox.getChildren().addAll(genrButton, saveButton);
    footBox.setSpacing(5);
    footBox.setAlignment(Pos.CENTER);
    setHgrow(footBox, Priority.ALWAYS);
    footBox.setStyle("-fx-background-color: #dcdcdc; -fx-padding: 5px;");

    window.add(footBox, 0, 2);

    // Center row, coordinate box & default affine transformation
    centerBox = new VBox();
    cordBox = createCoordinateBox();
    affiBox = createAffineBox(); // Default transformation,
    juliBox = createJuliaBox(); // Initialization
    Label loadInfo = new Label("Loaded files must have .txt format");
    loadInfo.setStyle(labelStyle);

    centerBox.getChildren().addAll(loadInfo, cordBox, affiBox);
    centerBox.setSpacing(40);
    centerBox.setAlignment(Pos.TOP_CENTER);
    centerBox.setPadding(new Insets(20, 10, 10, 10)); // (top, right, bottom, left
    centerBox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
    setHgrow(centerBox, Priority.ALWAYS);
    window.add(centerBox, 0, 1);

    // Set actions & load input
    setButtonActions();
    setInitialFields();

    // Create Scene
    Scene scene = new Scene(window, 300, 300);
    scene.getStylesheets().add(getClass().getResource("/styles.css").toExternalForm());
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  private void setButtonActions() {
    affiButton.setOnAction(e -> {
      transType = "Affine2D";
      centerBox.getChildren().remove(2); // Remove previous transformation box
      centerBox.getChildren().add(affiBox);
    });

    juliButton.setOnAction(e -> {
      transType = "Julia";
      centerBox.getChildren().remove(2); // Remove previous transformation box
      centerBox.getChildren().add(juliBox);
    });

    genrButton.setOnAction(e -> {
      primaryStage.close();
      updateFile();
      notifyObservers();
    });

    loadButton.setOnAction(e -> {
      try {
        fileAsString = FileHandler.readSelectionToString();
      } catch (DescriptionException ex) {
        MainWindow.displayErrorMessage(ex.getMessage());
      }
      if (!fileAsString.isEmpty()) {
        populateFieldsFromFile(fileAsString);
      }
    });

    saveButton.setOnAction(e -> saveInputNewFile());
  }

  /**
   * Set the initial fields for the overlay.
   *
   * @see FileHandler#dirtyReadToString()
   */
  private void setInitialFields() {
    fileAsString = FileHandler.dirtyReadToString();
    populateFieldsFromFile(fileAsString);
  }

  /**
   * Populate fields in the overlay with fractal information from a file.
   * Also alters layout of overlay based on transformation type.
   *
   * @param transAndCoord List of strings containing fractal information
   */
  private void populateFieldsFromFile(List<String[]> transAndCoord) {
    if (!transAndCoord.isEmpty()) {
      String transTypeHolder = transAndCoord.get(0)[0];
      descriptionCoordinates.get(0).setText(transAndCoord.get(1)[0].trim());
      descriptionCoordinates.get(1).setText(transAndCoord.get(1)[1].trim());
      descriptionCoordinates.get(2).setText(transAndCoord.get(2)[0].trim());
      descriptionCoordinates.get(3).setText(transAndCoord.get(2)[1].trim());

      if (transTypeHolder.equalsIgnoreCase("Affine2D")) {
        transType = transTypeHolder;
        centerBox.getChildren().remove(2); // Remove previous transformation box
        affiBox = createAffineBox();
        centerBox.getChildren().add(affiBox);
        affiInputText.setText(transAndCoord.get(3)[0].trim());
      }

      if (transTypeHolder.equalsIgnoreCase("Julia")) {
        transType = transTypeHolder;
        centerBox.getChildren().remove(2); // Remove previous transformation box
        juliBox = createJuliaBox();
        centerBox.getChildren().add(juliBox);
        juliaVectorFields.get(0).setText(transAndCoord.get(3)[0].trim());
        juliaVectorFields.get(1).setText(transAndCoord.get(3)[1].trim());
      }
    }
  }

  /**
   * Update the file with the latest input from the overlay.
   *
   * @see CustomInput#affineInput(String, List, TextArea)
   * @see CustomInput#juliaInput(String, List, List)
   * @see FileHandler#writeToFile(String)
   */
  private void updateFile() {
    if (transType.equalsIgnoreCase("Affine2D")) {
      String toFile = CustomInput.affineInput(transType, descriptionCoordinates, affiInputText);
      FileHandler.writeToFile(toFile);

    } else if (transType.equalsIgnoreCase("Julia")) {
      String toFile = CustomInput.juliaInput(transType, descriptionCoordinates, juliaVectorFields);
      FileHandler.writeToFile(toFile);
    }
  }

  /**
   * Save the input from the overlay to a new file.
   *
   * @see CustomInput#affineInput(String, List, TextArea)
   * @see CustomInput#juliaInput(String, List, List)
   * @see FileHandler#saveToFile(String)
   */
  private void saveInputNewFile() {
    if (transType.equalsIgnoreCase("Affine2D")) {
      String toFile = CustomInput.affineInput(transType, descriptionCoordinates, affiInputText);
      FileHandler.saveToFile(toFile);

    } else if (transType.equalsIgnoreCase("Julia")) {
      String toFile = CustomInput.juliaInput(transType, descriptionCoordinates, juliaVectorFields);
      FileHandler.saveToFile(toFile);
    }
  }

  private VBox createCoordinateBox() {
    descriptionCoordinates = new ArrayList<>();

    // Information
    Label coordInfo = new Label("Enter min and max coordinates for the fractal plane");
    coordInfo.setStyle(labelStyle);

    // Maximum coordinates input fields
    TextField xMaxField = new TextField();
    xMaxField.setPromptText("Max X");
    TextField yMaxField = new TextField();
    yMaxField.setPromptText("Max Y");

    // Minimum coordinates input fields
    TextField xMinField = new TextField();
    xMinField.setPromptText("Min X");
    TextField yMinField = new TextField();
    yMinField.setPromptText("Min Y");

    // Add fields to the list
    descriptionCoordinates.add(xMinField);
    descriptionCoordinates.add(yMinField);
    descriptionCoordinates.add(xMaxField);
    descriptionCoordinates.add(yMaxField);
    for (TextField field : descriptionCoordinates) {
      field.setPrefColumnCount(4);
    }

    // Maximum coordinates
    HBox maxBox = new HBox();
    maxBox.setSpacing(10);
    maxBox.setAlignment(Pos.CENTER);
    maxBox.getChildren().addAll(new Label("Max Coords:"), xMaxField, yMaxField);

    // Minimum coordinates
    HBox minBox = new HBox();
    minBox.setSpacing(10);
    minBox.setAlignment(Pos.CENTER);
    minBox.getChildren().addAll(new Label("Min Coords:"), xMinField, yMinField);

    // Coordinate box
    VBox coordinateBox = new VBox();
    coordinateBox.setSpacing(15);
    coordinateBox.setAlignment(Pos.CENTER);
    setHgrow(coordinateBox, Priority.ALWAYS);
    coordinateBox.getChildren().addAll(coordInfo, minBox, maxBox);

    return coordinateBox;
  }

  private VBox createJuliaBox() {
    juliBox = new VBox();

    // Information
    Label transInfo = new Label("Enter input for the Julia Transformation z —> z^2 + c");
    Label equationInfo = new Label("z  —>  z^2   +");
    transInfo.setStyle(labelStyle);
    equationInfo.setStyle(labelStyle);

    // Vector c
    juliaVectorFields = new ArrayList<>();
    TextField xField = new TextField();
    TextField yField = new TextField();
    xField.setPromptText("real part");
    yField.setPromptText("im part");
    juliaVectorFields.add(xField);
    juliaVectorFields.add(yField);

    HBox vectorBox = new HBox();
    vectorBox.setSpacing(10);
    vectorBox.setMaxSize(150, 150);
    vectorBox.setAlignment(Pos.CENTER);
    setHgrow(vectorBox, Priority.ALWAYS);
    vectorBox.getChildren().addAll(equationInfo, xField, yField);

    HBox labelAndVectorBox = new HBox();
    labelAndVectorBox.setSpacing(20);
    labelAndVectorBox.setAlignment(Pos.CENTER);
    setHgrow(vectorBox, Priority.ALWAYS);
    labelAndVectorBox.getChildren().addAll(equationInfo, vectorBox);

    juliBox.setSpacing(15);
    juliBox.setAlignment(Pos.CENTER);
    setHgrow(juliBox, Priority.ALWAYS);
    juliBox.getChildren().addAll(transInfo, labelAndVectorBox);
    return juliBox;
  }

  private VBox createAffineBox() {

    // Information
    Label transInfo = new Label("Enter input for the Affine Transformation A * x + b");
    transInfo.setStyle(labelStyle);

    // Tooltip info
    String tipText = "The input must be given in the format:"
        + "\n\nA00, A01, A10, A11, b0, b1\nA00, A01, A10, A11, b0, b1\n ect.";
    TipButton tipButton = new TipButton("?", tipText);
    tipButton.setMaxSize(30, 30);
    tipButton.setTooltipArea(275, 150);

    //Transformation input area
    affiBox = new VBox();
    affiInputBox = createAffineInputArea();
    affiBox.setAlignment(Pos.CENTER);
    setHgrow(affiBox, Priority.ALWAYS);
    affiBox.setSpacing(15);
    affiBox.getChildren().addAll(transInfo, tipButton, affiInputBox);
    return affiBox;
  }

  private HBox createAffineInputArea() {
    affiInputBox = new HBox();

    affiInputText = new TextArea();
    affiInputText.setPromptText("A00, A01, A10, A11, b0, b1 \nA00, A01, A10, A11, b0, b1 \n...");
    affiInputText.setMaxSize(300, Double.MAX_VALUE); // (width, height)
    affiInputText.setStyle("-fx-font-family: monospace; -fx-font-size: 16px;");
    GridPane.setHgrow(affiInputText, Priority.ALWAYS);
    GridPane.setVgrow(affiInputText, Priority.ALWAYS);

    affiInputBox.getChildren().addAll(affiInputText);
    affiInputBox.setAlignment(Pos.CENTER);
    affiInputBox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
    affiInputBox.setPadding(new Insets(0, 10, 0, 10)); // (top, right, bottom, left)
    return affiInputBox;
  }

  public String getTransType() {
    return transType;
  }

  public void addObserver(OverlayObserver observer) {
    observers.add(observer);
  }

  private void notifyObservers() {
    for (OverlayObserver observer : observers) {
      observer.customFractal();
    }
  }
}