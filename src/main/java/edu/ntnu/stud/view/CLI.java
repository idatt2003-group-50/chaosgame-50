package edu.ntnu.stud.view;

import edu.ntnu.stud.controller.FileHandler;
import edu.ntnu.stud.exception.DescriptionException;
import edu.ntnu.stud.model.Description;
import edu.ntnu.stud.model.Fractal;
import edu.ntnu.stud.model.FractalCanvas;
import java.util.Scanner;

/**
 * Simple command line interface for the Chaos Fractal application. Provides the options:
 * <ul>
 *     <li>Read description from file</li>
 *     <li>Write description to file</li>
 *     <li>Run iterations</li>
 *     <li>Print ASCII fractal</li>
 * </ul>
 */
public class CLI {
  private Description description;
  private FractalCanvas fractalCanvas;

  private final int readDescription = 1;
  private final int writeDescription = 2;
  private final int runIterations = 3;
  private final int printASCIIfractal = 4;
  private final int exitSelected = 5;

  /**
   * Starts the command line interface.
   */
  public void run() {
    Scanner scanner = new Scanner(System.in);

    boolean exit = false;
    while (!exit) {
      System.out.println("——————————— Menu ———————————");
      System.out.println("1. Read description from file");
      System.out.println("2. Write description to file");
      System.out.println("3. Run iterations");
      System.out.println("4. Print ASCII fractal");
      System.out.println("5. Exit");
      System.out.print("Choose an option: ");

      int choice = scanner.nextInt();
      scanner.nextLine();

      switch (choice) {
        case readDescription:
          readDescriptionFromFile();
          break;
        case writeDescription:
          writeDescriptionToFile();
          break;
        case runIterations:
          runIterations();
          break;
        case printASCIIfractal:
          printAsciiFractal();
          break;
        case exitSelected:
          exit = true;
          System.out.println("Exiting...");
          break;
        default:
          System.out.println("Invalid choice. Please choose a valid option.");
      }
    }
    scanner.close();
  }

  private void readDescriptionFromFile() {
    Scanner scanner = new Scanner(System.in);

    System.out.println("Choose a file to read description from:");
    System.out.println("1. Julia transformation");
    System.out.println("2. Sierpinski transformation");
    System.out.println("3. Bernsley transformation");
    System.out.print("Enter choice: ");

    int fileChoice = scanner.nextInt();

    String filePath = null;
    switch (fileChoice) {
      case 1:
        filePath = "src/main/resources/fractal/julia";
        break;
      case 2:
        filePath = "src/main/resources/fractal/affineSier.txt";
        break;
      case 3:
        filePath = "src/main/resources/fractal/affineBarn.txt";
        break;
      default:
        System.out.println("Invalid choice. Exiting...");
        scanner.close();
        return;
    }
    try {
      description = FileHandler.readToDescription(filePath);
      fractalCanvas = new FractalCanvas(
          50, 50, description.getMinCoords(), description.getMaxCoords());
      System.out.println("Reading description from file...");
    } catch (DescriptionException e) {
      System.out.println("Error reading description from file: " + e.getMessage());
    }
  }

  private void writeDescriptionToFile() {
    // Implement logic to write description to file
  }

  private void runIterations() {
    //user input for iterations?
    if (description == null) {
      System.out.println("Error: Description or not initialized.");
      return;
    }
    Fractal fractal = new Fractal(description, fractalCanvas);
    fractalCanvas.clearCanvas();

    System.out.print("Enter number of iterations: ");

    Scanner scan = new Scanner(System.in);
    String input = scan.nextLine();

    try {
      int steps = Integer.parseInt(input);
      fractal.runSteps(steps);
    } catch (NumberFormatException e) {
      System.out.println("Invalid input: " + e.getMessage());
    }
    System.out.println("Running iterations...");
  }

  private void printAsciiFractal() {
    if (description == null) {
      System.out.println("Error: Description or not initialized.");
      return;
    }
    int[][] canvasArray = fractalCanvas.getCanvasArray();

    int height = fractalCanvas.getHeight();
    int width = fractalCanvas.getWidth();

    // Iterate over columns instead of rows
    for (int j = 0; j < width; j++) {
      // Iterate over rows in reverse order
      for (int i = height - 1; i >= 0; i--) {
        if (canvasArray[i][j] >= 1) {
          System.out.print(" * "); // Print asterisk for 1
        } else {
          System.out.print("   "); // Print blank space for 0
        }
      }
      System.out.println();
    }
  }
}