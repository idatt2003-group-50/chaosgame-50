package edu.ntnu.stud.view;

import edu.ntnu.stud.model.Fractal;
import edu.ntnu.stud.model.FractalFactory;
import edu.ntnu.stud.view.components.MainWindow;
import edu.ntnu.stud.view.components.Overlay;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.LogManager;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Launches the application.
 */
public class AppLauncher extends Application {

  public static void launcher(String[] args) {
    launch(args);
  }

  /**
   * This method is the main entry point for JavaFX. Initializes:
   * <ul>
   *   <li>Descriptions for the fractals.</li>
   *   <li>Fractal</li>
   *   <li>Overlay</li>
   *   <li>MainWindow</li>
   *   <li>Primary Scene</li>
   *   <li>Logging</li>
   *   <li>Styles</li>
   * </ul>
   *
   * @param primaryStage The primary stage of the application.
   * @throws IOException IOException if there is an error reading the logging configuration.
   */
  @Override
  public void start(Stage primaryStage) throws IOException {
    FractalFactory.initializeDescriptions();
    Fractal fractal = new Fractal();

    Overlay overlay = new Overlay();
    MainWindow mainWindow = new MainWindow(fractal, overlay);

    overlay.addObserver(mainWindow);
    fractal.addObserver(mainWindow);

    Scene scene = new Scene(mainWindow.getWindow());

    LogManager.getLogManager().readConfiguration(
        getClass().getResourceAsStream("/logging.properties"));

    scene.getStylesheets().add(getClass().getResource("/styles.css").toExternalForm());

    primaryStage.setScene(scene);
    primaryStage.setTitle("Chaos Game");
    primaryStage.setMinHeight(800);
    primaryStage.setMinWidth(800);
    primaryStage.show();
  }
}