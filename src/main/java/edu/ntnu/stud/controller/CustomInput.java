package edu.ntnu.stud.controller;

import java.util.List;
import java.util.logging.Logger;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * Provides methods for formatting user input.
 */
public final class CustomInput {
  private static final Logger logger = Logger.getLogger(CustomInput.class.getName());

  private CustomInput() {
    // private constructor to prevent instantiation
  }

  /**
   * Formats user input for minimum and maximum coordinates.
   *
   * @param descriptionCoords User input for coordinates.
   * @return Formatted input for coordinates.
   */
  private static String coordInput(List<TextField> descriptionCoords) {
    String lowerLeft = String.join(",", descriptionCoords.get(0).getText().trim(),
        descriptionCoords.get(1).getText().trim());

    String upperRight = String.join(",", descriptionCoords.get(2).getText().trim(),
        descriptionCoords.get(3).getText().trim());

    return lowerLeft + "\n" + upperRight;
  }

  /**
   * Formats user input for Julia set.
   *
   * @param transType        Transformation type.
   * @param descriptionCoords User input for coordinates.
   * @param juliField        User input for Julia set.
   * @return Formatted input for Julia set.
   */
  public static String juliaInput(String transType, List<TextField> descriptionCoords,
                                  List<TextField> juliField) {

    String coords = coordInput(descriptionCoords);
    String cVector = String.join(",", juliField.get(0).getText().trim(),
        juliField.get(1).getText().trim());

    String juliaInput = String.join("\n", transType.trim(), coords.trim(), cVector.trim());

    logger.info("——INPUT STAGE——");
    logger.info(juliaInput);
    return juliaInput;
  }

  /**
   * Formats user input for Affine transformations.
   *
   * @param transType Transformation type.
   * @param descriptionCoords User input for coordinates.
   * @param affiInputText User input for Affine transformations.
   * @return Formatted input for Affine transformations.
   *
   */
  public static String affineInput(String transType, List<TextField> descriptionCoords,
                                   TextArea affiInputText) {

    String coordinates = coordInput(descriptionCoords);
    String[] transformations = affiInputText.getText().split("\n");
    String affineInput = String.join("\n", transType.trim(), coordinates.trim());
    for (String transformation : transformations) {
      affineInput = String.join("\n", affineInput, transformation.trim());
    }

    logger.info("——INPUT STAGE——");
    logger.info(affineInput);
    return affineInput;
  }
}