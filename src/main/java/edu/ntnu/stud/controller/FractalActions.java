package edu.ntnu.stud.controller;

import edu.ntnu.stud.model.FractalObserver;
import javafx.scene.image.WritableImage;

/**
 * Interface for fractal actions.
 * <p>Implemented by {@link edu.ntnu.stud.view.components.MainWindow}.</p>
 */
public interface FractalActions {
  void setSteps(int steps);

  void resetView();

  void zoomIn();

  void zoomOut();

  void setZoomFactor(double zoomFactor);

  void setColorIteration(boolean toggleColor);

  void saveImage();

  void moveView(double xNorm, double yNorm);

  WritableImage getWritableImage();

  void addObserver(FractalObserver observer);

  void setMandelScheme(String scheme);
}