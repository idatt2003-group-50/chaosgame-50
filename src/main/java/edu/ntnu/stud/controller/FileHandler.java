package edu.ntnu.stud.controller;

import edu.ntnu.stud.exception.DescriptionException;
import edu.ntnu.stud.model.Description;
import edu.ntnu.stud.model.linalg.AffineTransform2D;
import edu.ntnu.stud.model.linalg.JuliaTransform;
import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Transform2D;
import edu.ntnu.stud.model.linalg.Vector2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javafx.stage.FileChooser;

/**
 * Phrases a <code>Description</code> from or to a .txt file.* Provides methods for:
 *
 * <li>Parsing affine transformations
 * {@link FileHandler#parseAffineParams(String)}</li>

 * <li>Parsing Julia set parameters
 * {@link FileHandler#parseJuliaParams(String)}
 * </li>
 *
 * <li>Parsing 2D vector
 * {@link FileHandler#parseVector2D(String)}
 * </li>
 *
 * <li>
 *   Reading a <code>Description</code> from <code>.txt</code> file: {@link
 *   FileHandler#readToDescription(String)}
 * </li>
 *
 * <li>Selecting <code>.txt</code> file and reading to a list of strings
 *   {@link FileHandler#readSelectionToString()}
 * </li>
 *
 * <li>Reading <code>.txt</code> file to a list of strings:
 * {@link FileHandler#dirtyReadToString()}
 * </li>
 *
 * <li>Writing a <code>Description</code> to <code>.txt</code> file:
 * {@link FileHandler#writeToFile(String)}
 * </li>
 *
 * <li>Saving a <code>Description</code> to <code>.txt</code> file:
 * {@link FileHandler#saveToFile(String)}
 * </li>
 */
public final class FileHandler {
  public static final String CUSTOM_FRACTAL_PATH = "src/main/resources/fractal/customFractal.txt";
  private static final Logger logger = Logger.getLogger(FileHandler.class.getName());

  private FileHandler() {
    // private constructor to prevent instantiation
  }

  private static List<String> fileCleaner(String path) {
    List<String> cleanFile = new ArrayList<>();
    try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
      String line;
      while ((line = reader.readLine()) != null) {
        String[] parts = line.split("\n | #");
        String trimmedLine = parts[0].trim();
        if (!trimmedLine.isEmpty()) {
          cleanFile.add(trimmedLine);
        }
      }
    } catch (IOException e) {
      logger.info("——FILE STAGE——");
      logger.info("Error reading from file: " + e.getMessage());
    }
    return cleanFile;
  }

  private static List<String> selectFile() {
    List<String> cleanFile = new ArrayList<>();
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Resource File");
    fileChooser
        .getExtensionFilters()
        .addAll(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
    java.io.File file = fileChooser.showOpenDialog(null);
    if (file != null) {
      String path = file.getAbsolutePath();
      cleanFile = fileCleaner(path);
    }
    return cleanFile;
  }

  /**
   * Parses affine transformation parameters from a line in a .txt file.
   *
   * @param line A line from a .txt file containing affine transformation parameters.
   * @return An array of affine transformation parameters.
   */
  public static double[] parseAffineParams(String line) {
    String[] parts = line.split(",");
    if (parts.length != 6) {
      return null; // Invalid format, return null
    }
    try {
      double[] params = new double[6];
      for (int i = 0; i < 6; i++) {
        params[i] = Double.parseDouble(parts[i].trim());
      }
      return params;
    } catch (NumberFormatException e) {
      return null; // Invalid numeric format, return null
    }
  }

  /**
   * Parses Julia set parameters from a line in a .txt file.
   *
   * @param line A line from a .txt file containing Julia set parameters.
   * @return An array of Julia set parameters.
   */
  public static double[] parseJuliaParams(String line) {
    String[] parts = line.split(",");
    if (parts.length != 2) {
      return null; // Invalid format, return null
    }
    try {
      double[] juliaParams = new double[2];
      double a = Double.parseDouble(parts[0].trim());
      double b = Double.parseDouble(parts[1].trim());
      juliaParams[0] = a;
      juliaParams[1] = b;
      return juliaParams;
    } catch (NumberFormatException e) {
      return null; // Invalid numeric format, return null
    }
  }

  /**
   * Parses a 2D vector from a line in a .txt file.
   *
   * @param line A line from a .txt file containing a 2D vector.
   * @return A 2D vector.
   */
  public static Vector2D parseVector2D(String line) {
    String[] parts = line.split(",");
    if (parts.length != 2) {
      return null; // Invalid format, return null
    }
    try {
      double x = Double.parseDouble(parts[0].trim());
      double y = Double.parseDouble(parts[1].trim());
      return new Vector2D(x, y);
    } catch (NumberFormatException e) {
      return null; // Invalid numeric format, return null
    }
  }

  /**
   * Reads a <code>Description</code> from a .txt file.
   *
   * @param path The path to the .txt file.
   * @return A <code>Description</code> object.
   * @throws DescriptionException If the file contains invalid input.
   */
  public static Description readToDescription(String path) throws DescriptionException {
    List<String> cleanFile = fileCleaner(path);

    String transformType = cleanFile.get(0);
    if (!transformType.equals("Affine2D")
        && !transformType.equals("Julia")
        && !transformType.equals("Mandel")) {
      throw new DescriptionException("Invalid transformation type");
    }

    Vector2D minCoords = parseVector2D(cleanFile.get(1)); // minCoords is on the second line
    Vector2D maxCoords = parseVector2D(cleanFile.get(2)); // maxCoords is on the third line
    if (minCoords == null) {
      throw new DescriptionException("Invalid input in minimum coordinates");
    }
    if (maxCoords == null) {
      throw new DescriptionException("Invalid input in maximum coordinates");
    }

    List<Transform2D> transformations = new ArrayList<>();
    if (transformType.equals("Affine2D")) {
      for (int i = 3; i < cleanFile.size(); i++) {
        double[] params = parseAffineParams(cleanFile.get(i));
        if (params != null) {
          Matrix2x2 matrix = new Matrix2x2(params[0], params[1], params[2], params[3]);
          Vector2D vector = new Vector2D(params[4], params[5]);
          transformations.add(new AffineTransform2D(matrix, vector));
        } else {
          throw new DescriptionException(
              "Invalid input in Affine Transformation (line: " + (i - 2) + ")");
        }
      }
    }
    if (transformType.equals("Julia")) {
      for (int i = 3; i < cleanFile.size(); i++) {
        double[] params = parseJuliaParams(cleanFile.get(i));
        if (params != null) {
          double realPart = params[0];
          double imaginaryPart = params[1];
          Vector2D c = new Vector2D(realPart, imaginaryPart);
          transformations.add(new JuliaTransform(c));
        } else {
          throw new DescriptionException("Invalid input in Julia Transformation (c)");
        }
      }
    }
    return new Description(transformations, transformType, minCoords, maxCoords);
  }

  /**
   * Reads a selection from a .txt file.
   *
   * @return A list of strings containing the selection.
   * @throws DescriptionException If the file contains invalid input.
   */
  public static List<String[]> readSelectionToString() throws DescriptionException {
    List<String> cleanFile = selectFile();
    List<String[]> transAndCoord = new ArrayList<>();

    if (!cleanFile.isEmpty()) {
      String[] transformType = cleanFile.get(0).split("\n");
      if (!transformType[0].equals("Affine2D")
          && !transformType[0].equals("Julia")
          && !transformType[0].equals("Mandel")) {
        throw new DescriptionException("Invalid transformation type");
      }
      String[] minCoords = cleanFile.get(1).split(","); // minCoords is on the second line
      if (minCoords.length != 2) {
        throw new DescriptionException("Invalid input in minimum coordinates");
      }
      String[] maxCoords = cleanFile.get(2).split(","); // maxCoords is on the third line
      if (maxCoords.length != 2) {
        throw new DescriptionException("Invalid input in maximum coordinates");
      }

      transAndCoord.add(transformType);
      transAndCoord.add(minCoords);
      transAndCoord.add(maxCoords);

      if (transformType[0].equals("Affine2D")) {
        StringBuilder transFromFile = new StringBuilder();
        for (int i = 3; i < cleanFile.size(); i++) {
          transFromFile.append(cleanFile.get(i)).append("\n");
        }
        transAndCoord.add(new String[] {transFromFile.toString()});
      }
      if (transformType[0].equals("Julia")) {
        for (int i = 3; i < cleanFile.size(); i++) {
          String[] constant = cleanFile.get(i).split(",");
          transAndCoord.add(constant);
        }
      }
    }
    return transAndCoord;
  }

  /**
   * Reads a selection from a .txt file.
   *
   * @return A list of strings containing the selection.
   */
  public static List<String[]> dirtyReadToString() {
    List<String> cleanFile = fileCleaner(CUSTOM_FRACTAL_PATH);
    List<String[]> transAndCoord = new ArrayList<>();

    String[] transformType = cleanFile.get(0).split("\n");
    String[] minCoords = cleanFile.get(1).split(","); // minCoords is on the second line
    String[] maxCoords = cleanFile.get(2).split(","); // maxCoords is on the third line

    transAndCoord.add(transformType);
    transAndCoord.add(minCoords);
    transAndCoord.add(maxCoords);

    if (transformType[0].equals("Affine2D")) {
      StringBuilder transFromFile = new StringBuilder();
      for (int i = 3; i < cleanFile.size(); i++) {
        transFromFile.append(cleanFile.get(i)).append("\n");
      }
      transAndCoord.add(new String[] {transFromFile.toString()});
    }
    if (transformType[0].equals("Julia")) {
      for (int i = 3; i < cleanFile.size(); i++) {
        String[] constant = cleanFile.get(i).split(",");
        transAndCoord.add(constant);
      }
    }
    return transAndCoord;
  }

  /**
   * Writes a <code>Description</code> to a .txt file.
   *
   * @param fractalInfo A string containing the <code>Description</code> to be written.
   */
  public static void writeToFile(String fractalInfo) {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(CUSTOM_FRACTAL_PATH))) {
      writer.write(fractalInfo);
    } catch (IOException e) {
      logger.info("——FILE STAGE——");
      logger.info("Error writing to file: " + e.getMessage());
    }
  }

  /**
   * Saves a <code>Description</code> to a .txt file.
   *
   * @param fractalInfo A string containing the <code>Description</code> to be saved.
   */
  public static void saveToFile(String fractalInfo) {
    javafx.stage.FileChooser fileChooser = new javafx.stage.FileChooser();
    fileChooser.setTitle("Save File");
    fileChooser
        .getExtensionFilters()
        .addAll(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
    java.io.File file = fileChooser.showSaveDialog(null);
    if (file != null) {
      try (BufferedWriter writer = new BufferedWriter(new FileWriter(file.getAbsolutePath()))) {
        writer.write(fractalInfo);
      } catch (IOException e) {
        logger.info("——FILE STAGE——");
        logger.info("Error writing to file: " + e.getMessage());
      }
    }
  }
}
