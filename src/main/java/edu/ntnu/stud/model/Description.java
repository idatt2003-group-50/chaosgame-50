package edu.ntnu.stud.model;

import edu.ntnu.stud.model.linalg.AffineTransform2D;
import edu.ntnu.stud.model.linalg.JuliaTransform;
import edu.ntnu.stud.model.linalg.Transform2D;
import edu.ntnu.stud.model.linalg.Vector2D;
import java.util.List;

/**
 * Description of the fractal:
 * <ul>
 *   <li> Transformation type: <code>Affine2D</code>, <code>Julia</code> or <code>Mandel</code>.
 *   <li> List of transformation data: <code>Transform2D</code> objects. </li>
 *   <li> Coordinates of the fractal plane: <code>Vector2D</code> objects. </li>
 *   <ul> Original coordinates are stored as <code>final</code> in
 *   <code>limitMin</code> and <code>limitMax</code>. </ul>
 *   <ul> Changed coordinates are stored in <code>minCoords</code> and <code>maxCoords</code>. </ul>
 * </ul>
 *
 * @see Transform2D
 * @see Vector2D
 * @see AffineTransform2D AffineTransform2D
 * @see JuliaTransform JuliaTransform
 */
public class Description {
  private Vector2D minCoords;
  private Vector2D maxCoords;
  private final Vector2D limitMin;
  private final Vector2D limitMax;
  private final List<Transform2D> transformations;
  private final String transformationType;

  /**
   * Constructs a <code>Description</code> with the provided details.
   *
   * @param transformations List of <code>AffineTransform2D</code> <b>OR</b>
   *                        <code>JuliaTransform</code> objects.
   * @param transformationType Type of transformation: <code>Affine2D</code>,
   *                           <code>Julia</code> or <code>Mandel</code>.
   * @param minCoords The minimum coordinates of the fractal plane.
   * @param maxCoords The maximum coordinates of the fractal plane.
   */
  public Description(List<Transform2D> transformations, String transformationType,
                     Vector2D minCoords, Vector2D maxCoords) {
    this.transformations = transformations;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.limitMin = this.minCoords;
    this.limitMax = this.maxCoords;
    this.transformationType = transformationType;
  }

  /**
   * Returns a list of transformations.
   *
   * @return List of <code>AffineTransform2D</code> <b>OR</b> <code>JuliaTransform</code> objects
   *
   * @see Transform2D
   * @see AffineTransform2D AffineTransform2D
   * @see JuliaTransform JuliaTransform
   */
  public List<Transform2D> getTransformations() {
    return transformations;
  }

  public String getTransformationType() {
    return transformationType;
  }

  public void setMinCoords(Vector2D minCoords) {
    this.minCoords = minCoords;
  }

  public void setMaxCoords(Vector2D maxCoords) {
    this.maxCoords = maxCoords;
  }

  // Lower left corner
  public Vector2D getMinCoords() {
    return minCoords;
  }

  // Upper right corner
  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  // Original lower left corner
  public Vector2D getLimitMin() {
    return limitMin;
  }

  // Original upper right corner
  public Vector2D getLimitMax() {
    return limitMax;
  }
}