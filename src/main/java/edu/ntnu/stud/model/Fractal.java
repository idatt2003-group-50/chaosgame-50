package edu.ntnu.stud.model;

import edu.ntnu.stud.controller.FractalActions;
import edu.ntnu.stud.model.linalg.AffineTransform2D;
import edu.ntnu.stud.model.linalg.JuliaTransform;
import edu.ntnu.stud.model.linalg.Transform2D;
import edu.ntnu.stud.model.linalg.Vector2D;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;
import javax.imageio.ImageIO;
import org.apache.commons.math3.complex.Complex;

/**
 * Creates a <code>Fractal</code> based on the given description and canvas.
 *
 * <p>Implements {@link FractalActions}. </p>
 * Core functionalities:
 *<ul>
 *   <li>Creating affine image: {@link Fractal#createAffineImage(int)}</li>
 *   <li>Creating Julia image: {@link Fractal#createJuliaImage(int)}</li>
 *   <li>Creating Mandelbrot image: {@link Fractal#createMandelImage(int)}</li>
 *   <li>Zooming in: {@link Fractal#zoomIn()}</li>
 *   <li>Zooming out: {@link Fractal#zoomOut()}</li>
 *   <li>Centering the canvas: {@link Fractal#moveView(double, double)}</li>
 *   <li>Resetting the canvas: {@link Fractal#resetView()}</li>
 *   <li>Saving fractal image: {@link Fractal#saveImage()}</li>
 *   <li>Updating image and notifying observers: {@link Fractal#updateImageAndNotify()}</li>
 *</ul>
 *
 * @see Description
 * @see FractalCanvas
 * @see FractalObserver
 */
public class Fractal implements FractalActions {
  public static final double DEFAULT_ZOOM_FACTOR = 1.5;
  private int height;
  private int width;
  private int steps;
  private String mandelScheme;
  private boolean colorIteration;
  private double zoomFactor;
  private BufferedImage image;
  private FractalCanvas fractalCanvas;
  private Description description;
  private final Vector2D currentPoint;
  private final Random random = new Random();
  private static final Logger logger = Logger.getLogger(Fractal.class.getName());
  private final List<FractalObserver> observers = new ArrayList<>();

  /**
   * Constructs an initial <code>Fractal</code> object,
   * without <code>description</code> or <code>fractalCanvas</code>.
   * Initializes the current point to (0, 0) and the zoom factor to the default value.
   *
   * @see #setDescription(Description)
   * @see #setFractalCanvas(FractalCanvas)
   */
  public Fractal() {
    this.description = null;
    this.fractalCanvas = null;
    this.currentPoint = new Vector2D(0, 0); // Initialize current position to (0, 0)
    this.zoomFactor = DEFAULT_ZOOM_FACTOR;
  }

  /**
   * Constructs a <code>Fractal</code> with the given <code>description</code>
   * and <code>fractalCanvas</code>.
   *<p>Used for testing</p>
   *
   * @param description The description of the fractal.
   * @param fractalCanvas The canvas of the fractal.
   */
  public Fractal(Description description, FractalCanvas fractalCanvas) {
    this.description = description;
    this.fractalCanvas = fractalCanvas;
    this.currentPoint = new Vector2D(0, 0); // Initialize current position to (0, 0)
    this.height = fractalCanvas.getHeight();
    this.width = fractalCanvas.getWidth();
    this.zoomFactor = DEFAULT_ZOOM_FACTOR;
  }

  /**
   * Runs a number of steps for pixel generation.
   *
   * @param steps The number of steps to run.
   *
   * @see #createAffineImage(int)
   * @see #createJuliaImage(int)
   * @see #createMandelImage(int)
   */
  public void runSteps(int steps) {
    this.steps = steps;
    int count = 0;
    if (description.getTransformationType().equals("Affine2D")) {
      Transform2D randSelect = description.getTransformations().get(
          random.nextInt(description.getTransformations().size()));
      Vector2D iteratedPoint = randSelect.transform(currentPoint);

      while (count < steps) {
        Transform2D randSelect2 = description.getTransformations().get(
            random.nextInt(description.getTransformations().size()));
        iteratedPoint = randSelect2.transform(iteratedPoint);
        fractalCanvas.putPixel(iteratedPoint);
        count++;
      }
    }

    if (description.getTransformationType().equals("Julia")) {
      createJuliaImage(steps);
    }
    if (description.getTransformationType().equals("Mandel")) {
      createMandelImage(steps);
    }
  }

  /**
   * Creates an affine image with the given number of <code>steps</code>. The number of steps
   * influences how the affine transformation is applied to create the image.
   *
   * <p>Contains a <code>boolean</code> variable <code>colorIteration</code>
   * which may be set by  {@link Fractal#setColorIteration(boolean)}</p>
   * After the image is created, observers are notified of the change.
   *
   * @param steps the number of steps to use in the affine transformation
   *
   * @see AffineTransform2D AffineTransform2D
   * @see #notifyObservers()
   * @see FractalObserver
   * @see edu.ntnu.stud.view.components.MainWindow#update() MainWindow.update()
   */
  public void createAffineImage(int steps) {
    this.steps = steps;
    fractalCanvas.clearCanvas();
    runSteps(steps);

    image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D graphics = image.createGraphics();

    int[][] affineArray = fractalCanvas.getCanvasArray();

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        if (affineArray[y][x] == 0) {
          graphics.setColor(Color.BLACK);
        } else if (colorIteration && affineArray[y][x] >= 2) {
          graphics.setColor(new Color(139, 0, 0));
        } else if (affineArray[y][x] >= 1) {
          graphics.setColor(Color.WHITE);
        }
        graphics.drawRect(x, y, 1, 1);
      }
    }
    notifyObservers();
  }

  /**
   * Creates a Julia image with the given number of <code>steps</code>. The number of steps
   * influences how the Julia transformation is applied to create the image.
   * After the image is created, observers are notified of the change.
   *
   * <p>To improve color rendering, a <code>ratio</code> is calculated.
   * The color rendition was influenced by the work of Sharma Radhakrishna, found at:
   * <a href="https://github.com/CodeSpaceIndica/Java/blob/main/Mandlebrot/Mandlebrot.java">GitHub link</a></p>
   *
   * @param steps the number of steps to use in the Julia transformation
   *
   * @see JuliaTransform JuliaTransform
   * @see #indexToCoordinate(double, double)
   * @see ColorInterpolation#getJuliaColors(double)
   * @see #notifyObservers()
   * @see FractalObserver
   * @see edu.ntnu.stud.view.components.MainWindow#update() MainWindow.update()
   */
  public void createJuliaImage(int steps) {
    this.steps = steps;
    image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D graphics = image.createGraphics();
    Transform2D c = description.getTransformations().getFirst();

    double zAbsMax = 1.9;
    int maxCount = (int) (steps * 0.1);

    for (int ix = 0; ix < width; ix++) {
      for (int iy = 0; iy < height; iy++) {
        int count = 0;

        // Map the pixel index to the complex plane
        Vector2D z = indexToCoordinate(ix, iy);

        // Iterate the transformation z -> z^2 + c
        while (z.magnitude() <= zAbsMax && count < steps) {
          z = c.transform(z);
          count++;
        }
        // Update maxCount if necessary
        if (count > maxCount) {
          maxCount = count;
        }

        double ratio = count / (double) maxCount;
        graphics.setColor(ColorInterpolation.getJuliaColors(ratio));
        graphics.drawRect(ix, iy, 1, 1);
      }
    }
    notifyObservers();
  }

  /**
   * Creates a Mandelbrot image with the given number of <code>steps</code>. The number of steps
   * influences how the Mandelbrot function is applied to create the image.
   * After the image is created, observers are notified of the change.
   * <p>
   *   Contains a <code>String</code> variable <code>mandelScheme</code>
   *   which may be set by {@link Fractal#setMandelScheme(String)}.
   * </p>
   *
   * <p>To improve color rendering, a <code>ratio</code> is calculated.
   * The color rendition was influenced by the work of Sharma Radhakrishna, found at:
   * <a href="https://github.com/CodeSpaceIndica/Java/blob/main/Mandlebrot/Mandlebrot.java">GitHub link</a></p>
   *
   * @param steps the number of steps to use in the Mandelbrot function
   *
   * @see org.apache.commons.math3.complex.Complex Complex
   * @see ColorInterpolation#getMandelColors(double, String)
   * @see #notifyObservers()
   * @see FractalObserver
   * @see edu.ntnu.stud.view.components.MainWindow#update() MainWindow.update()
   */
  public void createMandelImage(int steps) {
    this.steps = steps;
    image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    Graphics2D graphics = image.createGraphics();

    double zAbsMax = 1.9;
    int maxCount = (int) (steps * 0.1);

    for (int ix = 0; ix < width; ix++) {
      for (int iy = 0; iy < height; iy++) {
        int count = 0;

        // Map the pixel index to the complex plane
        double xc = description.getMinCoords().getX() + (
            description.getMaxCoords().getX() - description.getMinCoords().getX()) * ix / width;
        double yc = description.getMinCoords().getY() + (
            description.getMaxCoords().getY() - description.getMinCoords().getY()) * iy / height;
        Complex c = new Complex(xc, yc);

        // Initialize z to (0,0)
        Complex z = Complex.ZERO;

        // Iterate the Mandelbrot function z -> z^2 + c
        while (z.abs() <= zAbsMax && count < steps) {
          z = z.multiply(z).add(c);
          count++;
        }

        // Update maxCount if necessary
        if (count > maxCount) {
          maxCount = count;
        }

        double ratio = count / (double) maxCount;
        graphics.setColor(ColorInterpolation.getMandelColors(ratio, mandelScheme));
        graphics.drawRect(ix, iy, 1, 1);
      }
    }
    notifyObservers();
  }

  /**
   * Converts the index of an element in the
   * <code>fractalCanvas</code> to a coordinate on the complex plane.
   *
   * @param ix The x-coordinate of the pixel index.
   * @param iy The y-coordinate of the pixel index.
   * @return <code>Vector2D</code> containing coordinates on the complex plane.
   *
   * @see FractalCanvas
   * @see Description
   */
  private Vector2D indexToCoordinate(double ix, double iy) {
    double zx = ix / (double) width * (description.getMaxCoords().getX()
        - description.getMinCoords().getX()) + description.getMinCoords().getX();

    double zy = iy / (double) height * (description.getMaxCoords().getY()
        - description.getMinCoords().getY()) + description.getMinCoords().getY();

    return new Vector2D(zx, zy);
  }

  /**
   * Updates the image and notifies observers.
   * <p>
   *   Logs the transformation type and minimum & maximum coordinates of the canvas and description.
   * </p>
   *
   */
  private void updateImageAndNotify() {
    switch (description.getTransformationType()) {
      case "Affine2D" -> createAffineImage(steps);
      case "Julia" -> createJuliaImage(steps);
      case "Mandel" -> createMandelImage(steps);
      default -> logger.info("Invalid transformation type: " + description.getTransformationType());
    }
    logger.info("——RENDER STAGE——");
    logger.info(description.getTransformationType());
    logger.info("minCoords: Canvas: [" + fractalCanvas.getMinCoords() + "], Description: ["
        + description.getMinCoords() + "]");
    logger.info("maxCoords: Canvas: [" + fractalCanvas.getMaxCoords() + "], Description: ["
        + description.getMaxCoords() + "]");
    logger.info("limitMin: Canvas: [" + fractalCanvas.getLimitMin() + "], Description: ["
        + description.getLimitMin() + "]");
    logger.info("limitMax: Canvas: [" + fractalCanvas.getLimitMax() + "], Description: ["
        + description.getLimitMax() + "]");
  }

  /**
   * Updates the description and canvas based on the current <code>fractalCanvas</code>.
   */
  private void updateDescriptionAndCanvas() {
    Vector2D minCoords = fractalCanvas.getMinCoords();
    Vector2D maxCoords = fractalCanvas.getMaxCoords();
    description.setMinCoords(minCoords);
    description.setMaxCoords(maxCoords);
  }

  /**
   * Resets the <code>fractalCanvas</code> based on
   * the initial limit coordinates of <code>description</code>.
   *
   * <p> Updates the image and notifies observers. </p>
   *
   * @see FractalCanvas#resetView(Vector2D, Vector2D)
   * @see #updateImageAndNotify()
   */
  @Override
  public void resetView() {
    fractalCanvas.resetView(description.getLimitMin(), description.getLimitMax());
    description.setMaxCoords(description.getLimitMax());
    description.setMinCoords(description.getLimitMin());
    updateImageAndNotify();
  }

  /**
   * Calculates and sets new dimensions of <code>fractalCanvas</code> after zooming in.
   *
   * <p> Contains a <code>double</code> variable <code>zoomFactor</code>
   * which may be set by {@link Fractal#setZoomFactor(double)} </p>
   *
   * @see #DEFAULT_ZOOM_FACTOR
   * @see #updateDescriptionAndCanvas()
   * @see #updateImageAndNotify()
   */
  @Override
  public void zoomIn() {
    Vector2D currentMinCoords = fractalCanvas.getMinCoords();
    Vector2D currentMaxCoords = fractalCanvas.getMaxCoords();

    // Calculate the center of the current view
    double centerX = (currentMaxCoords.getX() + currentMinCoords.getX()) / 2.0;
    double centerY = (currentMaxCoords.getY() + currentMinCoords.getY()) / 2.0;

    // Calculate the new dimensions after zooming
    double deltaX = (currentMaxCoords.getX() - currentMinCoords.getX()) / zoomFactor;
    double deltaY = (currentMaxCoords.getY() - currentMinCoords.getY()) / zoomFactor;

    // Calculate the new min and max coordinates
    double xMinMap = centerX - deltaX / 2.0;
    double yMinMap = centerY - deltaY / 2.0;
    double xMaxMap = centerX + deltaX / 2.0;
    double yMaxMap = centerY + deltaY / 2.0;

    // Set the new coordinates
    fractalCanvas.setMinCoords(new Vector2D(xMinMap, yMinMap));
    fractalCanvas.setMaxCoords(new Vector2D(xMaxMap, yMaxMap));

    logger.info("Zoomfactor: " + zoomFactor);
    updateDescriptionAndCanvas();
    updateImageAndNotify();
  }

  /**
   * Calculates and sets new dimensions of <code>fractalCanvas</code> after zooming out.
   *
   * <p> Contains a <code>double</code> variable <code>zoomFactor</code>
   * which may be set by {@link Fractal#setZoomFactor(double)} </p>
   *
   * @see #DEFAULT_ZOOM_FACTOR
   * @see #updateDescriptionAndCanvas()
   * @see #updateImageAndNotify()
   */
  @Override
  public void zoomOut() {
    Vector2D currentMinCoords = fractalCanvas.getMinCoords();
    Vector2D currentMaxCoords = fractalCanvas.getMaxCoords();

    // Calculate the center of the current view
    double centerX = (currentMaxCoords.getX() + currentMinCoords.getX()) / 2.0;
    double centerY = (currentMaxCoords.getY() + currentMinCoords.getY()) / 2.0;

    // Calculate the new dimensions after zooming
    double deltaX = (currentMaxCoords.getX() - currentMinCoords.getX()) * zoomFactor;
    double deltaY = (currentMaxCoords.getY() - currentMinCoords.getY()) * zoomFactor;

    // Calculate the new min and max coordinates
    double xMinMap = centerX - deltaX / 2.0;
    double yMinMap = centerY - deltaY / 2.0;
    double xMaxMap = centerX + deltaX / 2.0;
    double yMaxMap = centerY + deltaY / 2.0;

    fractalCanvas.setMaxCoords(new Vector2D(xMaxMap, yMaxMap));
    fractalCanvas.setMinCoords(new Vector2D(xMinMap, yMinMap));

    logger.info("Zoomfactor: " + zoomFactor);
    updateDescriptionAndCanvas();
    updateImageAndNotify();
  }

  /**
   * Centers the canvas based on the normalized coordinates of a mouse click.
   *
   * <p>Calls the appropriate {@link FractalCanvas}
   * centering methods based on the transformation type. </p>
   *
   * @param xNorm normalized x-coordinate of the mouse click.
   * @param yNorm normalized y-coordinate of the mouse click.
   *
   * @see FractalCanvas#moveViewAffine(double, double)
   * @see FractalCanvas#moveViewComplex(double, double)
   * @see #updateDescriptionAndCanvas()
   * @see #updateImageAndNotify()
   */
  @Override
  public void moveView(double xNorm, double yNorm) {
    if (description.getTransformationType().equals("Affine2D")) {
      fractalCanvas.moveViewAffine(xNorm, yNorm);
    } else {
      fractalCanvas.moveViewComplex(xNorm, yNorm);
    }
    logger.info("Click coordinates: xNorm: [" + xNorm + "], yNorm: [" + yNorm + "]");
    updateDescriptionAndCanvas();
    updateImageAndNotify();
  }

  /**
   * Saves fractal image as a .png file.
   */
  @Override
  public void saveImage() {
    javafx.stage.FileChooser fileChooser = new javafx.stage.FileChooser();
    fileChooser.setTitle("Save Image");
    fileChooser.getExtensionFilters().add(
        new javafx.stage.FileChooser.ExtensionFilter("PNG", "*.png"));
    java.io.File file = fileChooser.showSaveDialog(null);
    if (file != null) {
      try {
        ImageIO.write(image, "png", file);
        logger.info("Image saved successfully: " + file.getAbsolutePath());
      } catch (IOException e) {
        logger.info("Error saving image: " + e.getMessage());
      }
    }
  }

  @Override
  public void setSteps(int steps) {
    this.steps = steps;
    updateImageAndNotify();
  }

  /**
   * Sets the  <code>colorIteration</code> variable of {@link Fractal#createAffineImage(int)}.
   *
   * <p>Updates the image and notifies observers.</p>
   *
   * @param colorIteration true if color iteration is enabled, false otherwise.
   *
   * @see #updateImageAndNotify()
   */
  @Override
  public void setColorIteration(boolean colorIteration) {
    this.colorIteration = colorIteration;
    updateImageAndNotify();
  }

  @Override
  public void setMandelScheme(String scheme) {
    this.mandelScheme = scheme;
    updateImageAndNotify();
  }

  /**
   * Sets the <code>zoomFactor</code> variable of
   * {@link Fractal#zoomIn()} & {@link Fractal#zoomOut()}.
   *
   * @param zoomFactor new zoom factor
   *
   * @see Fractal#DEFAULT_ZOOM_FACTOR
   */
  @Override
  public void setZoomFactor(double zoomFactor) {
    this.zoomFactor = zoomFactor;
  }

  /**
   * Converts and returns the <code>BufferedImage</code> of the
   * <code>fractal</code> a <code>WritableImage</code>.
   *
   * @return <code>WritableImage</code>
   */
  @Override
  public WritableImage getWritableImage() {
    return SwingFXUtils.toFXImage(this.image, null);
  }

  @Override
  public void addObserver(FractalObserver observer) {
    observers.add(observer);
  }

  private void notifyObservers() {
    for (FractalObserver observer : observers) {
      observer.update();
    }
  }

  /**
   * Sets the <code>FractalCanvas</code> for an initialized <code>Fractal</code>.
   *
   * @param fractalCanvas new fractalCanvas.
   */
  public void setFractalCanvas(FractalCanvas fractalCanvas) {
    this.fractalCanvas = fractalCanvas;
    this.height = fractalCanvas.getHeight();
    this.width = fractalCanvas.getWidth();
  }

  /**
   * Sets the <code>Description</code> for an initialized <code>Fractal</code>.
   *
   * @param description new description.
   */
  public void setDescription(Description description) {
    this.description = description;
  }

  public Description getDescription() {
    return description;
  }

  public FractalCanvas getCanvas() {
    return fractalCanvas;
  }

  public int getSteps() {
    return steps;
  }

  public boolean getColorIteration() {
    return colorIteration;
  }
}