package edu.ntnu.stud.model;

import edu.ntnu.stud.model.linalg.AffineTransform2D;
import edu.ntnu.stud.model.linalg.Matrix2x2;
import edu.ntnu.stud.model.linalg.Vector2D;
import java.util.logging.Logger;

/**
 * Represents the canvas where the points of the transformations are drawn.
 * The canvas is as a 2D array where each cell contains a number representing a pixel.
 *
 * <p>Core functionalities:
 * <ul>
 *   <li>{@link #putPixel(Vector2D)}: Places a pixel on the canvas at the specified point.
 *   <li>{@link #clearCanvas()}: Clears the canvas by setting all elements to 0.
 *   <li>{@link #moveViewAffine(double, double)}: Centers a canvas for Affine transformations
 *   <li>{@link #moveViewComplex(double, double)}: Centers a canvas for complex transformations
 *   <li>{@link #resetView(Vector2D, Vector2D)}:
 *   Resets the canvas coordinates to the initial values.
 * </ul>
 *
 * @see AffineTransform2D
 * @see Matrix2x2
 * @see Vector2D
 */
public class FractalCanvas {
  private final int[][] canvas;
  private final int width;
  private final int height;
  private Vector2D minCoords;
  private Vector2D maxCoords;
  private Vector2D limitMin;
  private Vector2D limitMax;
  private static final Logger logger = Logger.getLogger(FractalCanvas.class.getName());

  /**
   * Constructs a <code>FractalCanvas</code> with the provided details.
   * <p>The initial minimum & maximum coordinates are stored as
   * <code>limitMin</code> and <code>limitMax</code>. <b>These are subject to change</b>.
   * <code>final</code> versions are maintained in {@link Description}</p>
   *
   * @param width     The width of the canvas.
   * @param height    The height of the canvas.
   * @param minCoords The minimum coordinates of the canvas.
   * @param maxCoords The maximum coordinates of the canvas.
   */
  public FractalCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    this.width = width;
    this.height = height;
    this.canvas = new int[height][width];
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.limitMin = this.minCoords;
    this.limitMax = this.maxCoords;
  }

  /**
   * Transforms coordinates to indices in the canvas array.
   *
   * @return An affine transformation that maps coordinates to indices.
   *
   * @see AffineTransform2D
   * @see Matrix2x2
   * @see Vector2D
   */
  public AffineTransform2D coordinatesToIndex() {
    int M = height;
    int N = width;

    int a00 = 0;
    double a01 = (M - 1) / (minCoords.getY() - maxCoords.getY());
    double a10 = (N - 1) / (maxCoords.getX() - minCoords.getX());
    int a11 = 0;

    double bx = ((M - 1) * maxCoords.getY()) / (maxCoords.getY() - minCoords.getY());
    double by = ((N - 1) * minCoords.getX()) / (minCoords.getX() - maxCoords.getX());

    Matrix2x2 A = new Matrix2x2(a00, a01, a10, a11);
    Vector2D b = new Vector2D(bx, by);
    return new AffineTransform2D(A, b);
  }

  /**
   * Puts a pixel on the canvas at the specified point.
   *
   * @param point the coordinates of the point to put a pixel.
   * @see #coordinatesToIndex()
   */
  public void putPixel(Vector2D point) {
    Vector2D index = coordinatesToIndex().transform(point);

    int x = (int) Math.round(index.getX());
    int y = (int) Math.round(index.getY());

    // Check if the calculated indices are within bounds of the canvas
    if (0 < y && y < height && 0 < x && x < width) {
      canvas[x][y] += 1;
    }
  }

  /**
   * Centers the canvas for Affine transformations
   * based on the normalized coordinates of a mouse click.
   *
   * @param xNorm normalized x-coordinate of the mouse click
   * @param yNorm normalized y-coordinate of the mouse click
   */
  public void moveViewAffine(double xNorm, double yNorm) {
    // Calculate the translation (to the center of the canvas) using point defined by mouse click
    double translateX = xNorm - 0.5;
    double translateY = 0.5 - yNorm;

    // Calculate the new center coordinates
    double newCenterX = (maxCoords.getX() + minCoords.getX())
        / 2.0 + translateX * (maxCoords.getX() - minCoords.getX());

    double newCenterY = (maxCoords.getY() + minCoords.getY())
        / 2.0 + translateY * (maxCoords.getY() - minCoords.getY());

    // Ensure the new center coordinates are within the initial bounds
    double absoluteMinX = limitMin.getX();
    double absoluteMinY = limitMin.getY();
    double absoluteMaxX = limitMax.getX();
    double absoluteMaxY = limitMax.getY();

    if (newCenterX < absoluteMinX || newCenterX > absoluteMaxX) {
      newCenterX = Math.min(Math.max(newCenterX, absoluteMinX), absoluteMaxX);
    }
    if (newCenterY < absoluteMinY || newCenterY > absoluteMaxY) {
      newCenterY = Math.min(Math.max(newCenterY, absoluteMinY), absoluteMaxY);
    }

    // Calculate the new minCoords and maxCoords based on the new center
    double halfWidth = (maxCoords.getX() - minCoords.getX()) / 2.0;
    double halfHeight = (maxCoords.getY() - minCoords.getY()) / 2.0;

    this.minCoords = new Vector2D(newCenterX - halfWidth, newCenterY - halfHeight);
    this.maxCoords = new Vector2D(newCenterX + halfWidth, newCenterY + halfHeight);

    logger.info("——CENTER STAGE——");
    logCoordinates(minCoords, maxCoords, limitMin, limitMax);
  }

  /**
   * Centers the canvas for complex transformations (Julia or Mandelbrot)
   * based on the normalized coordinates of a mouse click.
   *
   * @param xNorm normalized x-coordinate of the mouse click
   * @param yNorm normalized y-coordinate of the mouse click
   */
  public void moveViewComplex(double xNorm, double yNorm) {
    // Calculate the translation (to the center of the canvas) using point defined by mouse click
    double translateX = xNorm - 0.5;
    double translateY = 0.5 - yNorm;

    double newCenterX;
    double newCenterY;

    // Calculate the new center coordinates for the X-axis
    if (minCoords.getX() < 0.0) {
      double positiveTranslateX = Math.max(translateX, 0.0);
      double negativeTranslateX = Math.min(translateX, 0.0);

      newCenterX = (maxCoords.getX() + minCoords.getX())
          / 2.0 + positiveTranslateX * (maxCoords.getX() - minCoords.getX());

      newCenterX += negativeTranslateX * (maxCoords.getX() - minCoords.getX());
    } else {
      newCenterX = (maxCoords.getX() + minCoords.getX())
          / 2.0 + translateX * (maxCoords.getX() - minCoords.getX());
    }

    // Calculate the new center coordinates for the Y-axis
    if (minCoords.getY() < 0.0) {
      double positiveTranslateY = Math.max(translateY, 0.0);
      double negativeTranslateY = Math.min(translateY, 0.0);

      newCenterY = (maxCoords.getY() + minCoords.getY())
          / 2.0 - positiveTranslateY * (maxCoords.getY() - minCoords.getY());

      newCenterY += negativeTranslateY * (minCoords.getY() - maxCoords.getY());
    } else {
      newCenterY = (maxCoords.getY() + minCoords.getY())
          / 2.0 - translateY * (maxCoords.getY() - minCoords.getY());
    }

    // Ensure the new center coordinates are within the initial bounds
    double absoluteMinX = limitMin.getX();
    double absoluteMinY = limitMin.getY();
    double absoluteMaxX = limitMax.getX();
    double absoluteMaxY = limitMax.getY();

    newCenterX = Math.min(Math.max(newCenterX, absoluteMinX), absoluteMaxX);
    newCenterY = Math.min(Math.max(newCenterY, absoluteMinY), absoluteMaxY);

    // Calculate the new minCoords and maxCoords based on the new center
    double halfWidth = (maxCoords.getX() - minCoords.getX()) / 2.0;
    double halfHeight = (maxCoords.getY() - minCoords.getY()) / 2.0;

    this.minCoords = new Vector2D(newCenterX - halfWidth, newCenterY - halfHeight);
    this.maxCoords = new Vector2D(newCenterX + halfWidth, newCenterY + halfHeight);

    logger.info("——CENTER STAGE——");
    logCoordinates(minCoords, maxCoords, limitMin, limitMax);
  }

  /**
   * Resets the canvas coordinates to the initial values.
   *
   * @param resetMin initial minimum coordinates of the description.
   * @param resetMax initial maximum coordinates of the description.
   */
  public void resetView(Vector2D resetMin, Vector2D resetMax) {
    logger.info("——RESET STAGE——");
    logger.info("Before");
    logCoordinates(minCoords, maxCoords, limitMin, limitMax);

    this.minCoords = resetMin;
    this.maxCoords = resetMax;
    this.limitMin = resetMin;
    this.limitMax = resetMax;

    logger.info("After");
    logCoordinates(minCoords, maxCoords, limitMin, limitMax);
  }

  private void logCoordinates(
      Vector2D minCoords, Vector2D maxCoords, Vector2D limitMin, Vector2D limitMax) {
    logger.info("Canvas minCoords: [" + minCoords + "]");
    logger.info("Canvas maxCoords: [" + maxCoords + "]");
    logger.info("Canvas limitMinCoords: [" + limitMin + "]");
    logger.info("Canvas limitMaxCoords: [" + limitMax + "]");
  }

  /**
   * Clears the canvas by setting all elements to 0.
   */
  public void clearCanvas() {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        canvas[i][j] = 0;
      }
    }
  }

  public int[][] getCanvasArray() {
    return canvas;
  }

  public int getHeight() {
    return height;
  }

  public int getWidth() {
    return width;
  }

  public void setMaxCoords(Vector2D maxCoords) {
    this.maxCoords = maxCoords;
  }

  public void setMinCoords(Vector2D minCoords) {
    this.minCoords = minCoords;
  }

  public Vector2D getMinCoords() {
    return minCoords;
  }

  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  public Vector2D getLimitMin() {
    return limitMin;
  }

  public Vector2D getLimitMax() {
    return limitMax;
  }
}