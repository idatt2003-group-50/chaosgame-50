package edu.ntnu.stud.model;

import java.awt.Color;
import java.util.Random;

/**
 * Defines colors based on ranges and values. Used for Julia and Mandelbrot sets.
 *
 */
public final class ColorInterpolation {
  private ColorInterpolation() {
    // private constructor to prevent instantiation
  }

  private static final double[][] JULIA_RANGES = {
      {0.0, 0.1},
      {0.1, 0.2},
      {0.3, 0.4},
      {0.4, 0.5},
      {0.5, 0.6},
      {0.6, 0.7},
      {0.7, 0.8},
      {0.8, 0.9},
      {0.9, 1.0}
  };

  private static final Color[] JULIA_COLORS = {
      new Color(0, 0, 0),
      new Color(0, 0, 0),
      new Color(255, 0, 0),
      new Color(255, 166, 0),
      new Color(255, 236, 21),
      new Color(245, 245, 245),
      Color.WHITE,
      Color.WHITE,
      Color.WHITE
  };

  /**
   * Returns a color based on the value of an element in the Julia canvas.
   *
   * @param value value of the element.
   * @return color of the element.
   */
  public static Color getJuliaColors(double value) {
    // Interpolate between colors based on value
    for (int i = 0; i < JULIA_RANGES.length - 1; i++) {
      double[] range = JULIA_RANGES[i];
      double[] nextRange = JULIA_RANGES[i + 1];
      if (value >= range[0] && value < nextRange[0]) {
        double ratio = (value - range[0]) / (nextRange[0] - range[0]);
        Color color1 = JULIA_COLORS[i];
        Color color2 = JULIA_COLORS[i + 1];
        int red = (int) (color1.getRed() * (1 - ratio) + color2.getRed() * ratio);
        int green = (int) (color1.getGreen() * (1 - ratio) + color2.getGreen() * ratio);
        int blue = (int) (color1.getBlue() * (1 - ratio) + color2.getBlue() * ratio);
        return new Color(red, green, blue);
      }
    }
    // Default color if value is outside defined ranges
    return Color.BLACK;
  }

  private static final double[][] MANDEL_RANGES = {
      {0.0, 0.1},
      {0.1, 0.2},
      {0.3, 0.4},
      {0.4, 0.5},
      {0.5, 0.6},
      {0.6, 0.7},
      {0.7, 0.8},
      {0.8, 0.9},
      {0.9, 1.0}
  };

  private static final Color[] APOLLO = {
      new Color(0, 0, 0),
      new Color(225, 0, 0, 255),
      new Color(255, 145, 0),
      new Color(255, 250, 0),
      new Color(0, 255, 217),
      new Color(0, 166, 255),
      new Color(255, 255, 255),
      new Color(2, 225, 255),
      new Color(255, 255, 255),
      new Color(159, 0, 3)
  };

  private static final Color[] ETHER = {
      new Color(0, 0, 0),
      new Color(0, 255, 149),
      new Color(0, 225, 255),
      new Color(0, 32, 136),
      new Color(1, 255, 255),
      new Color(189, 63, 255),
      new Color(132, 0, 255),
      new Color(214, 0, 0),
      new Color(0, 34, 255),
      new Color(132, 0, 255),
  };

  private static final Color[] BLACKWHITE = {
      new Color(0, 0, 0),
      new Color(255, 255, 255),
      new Color(0, 0, 0),
      new Color(255, 255, 255),
      new Color(0, 0, 0),
      new Color(255, 255, 255),
      new Color(0, 0, 0),
      new Color(255, 255, 255),
      new Color(0, 0, 0),
      new Color(255, 255, 255)
  };

  private static Color[] generateRandomColors(int length) {
    Random random = new Random();
    Color[] colors = new Color[length];
    for (int i = 0; i < length; i++) {
      int red = random.nextInt(256);
      int green = random.nextInt(256);
      int blue = random.nextInt(256);
      colors[i] = new Color(red, green, blue);
    }
    return colors;
  }

  /**
   * Returns a color based on the value of an element in the Mandelbrot canvas
   * and the selected color scheme.
   *
   * @param value value of the element.
    * @param scheme color scheme to use.
   * @return color of the element.
   */
  public static Color getMandelColors(double value, String scheme) {
    Color[] colors;
    colors = switch (scheme) {
      case "Static" -> generateRandomColors(10);
      case "Apollo" -> APOLLO;
      case "B&W" -> BLACKWHITE;
      case "Ether" -> ETHER;
      case null -> ETHER;
      default -> ETHER;
    };

    // Interpolate between colors based on value
    for (int i = 0; i < MANDEL_RANGES.length - 1; i++) {
      double[] range = MANDEL_RANGES[i];
      double[] nextRange = MANDEL_RANGES[i + 1];
      if (value >= range[0] && value < nextRange[0]) {
        double ratio = (value - range[0]) / (nextRange[0] - range[0]);
        Color color1 = colors[i];
        Color color2 = colors[i + 1];
        int red = (int) (color1.getRed() * (1 - ratio) + color2.getRed() * ratio);
        int green = (int) (color1.getGreen() * (1 - ratio) + color2.getGreen() * ratio);
        int blue = (int) (color1.getBlue() * (1 - ratio) + color2.getBlue() * ratio);
        return new Color(red, green, blue);
      }
    }
    // Default color if value is outside defined ranges
    return Color.BLACK;
  }
}