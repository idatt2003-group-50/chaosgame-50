package edu.ntnu.stud.model;

/**
 * Observer interface for <code>fractal</code> changes.
 * <p> Implemented by {@link edu.ntnu.stud.view.components.MainWindow MainWindow}.</p>
 *
 * @see Fractal
 */
public interface FractalObserver {
  void update();
}