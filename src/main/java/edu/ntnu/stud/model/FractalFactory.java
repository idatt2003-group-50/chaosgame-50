package edu.ntnu.stud.model;

import edu.ntnu.stud.controller.FileHandler;
import edu.ntnu.stud.exception.DescriptionException;
import java.util.logging.Logger;

/**
 * Creates <code>Description</code>, <code>FractalCanvas</code> <code>fractalImage</code> based on
 * predefined parameters and updates <code>Fractal</code> with the created objects.
 *
 * <p>Provides methods for:
 * <ul>Initializing the factory with descriptions: {@link FractalFactory#initializeDescriptions()}
 * </ul>
 * <ul>Creating a Sierpinski triangle: {@link FractalFactory#createSierpinski(Fractal)}
 * </ul>
 * <ul>Creating a Barnsley fern: {@link FractalFactory#createBarnsley(Fractal)}
 * </ul>
 * <ul>Creating a Julia set: {@link FractalFactory#createJulia(Fractal, String)}
 * </ul>
 * <ul>Creating a Mandelbrot set: {@link FractalFactory#createMandelbrot(Fractal)}
 * </ul>
 * <ul>Creating a custom fractal: {@link FractalFactory#createCustom(Fractal)}
 * </ul>
 *
 * @see Description
 * @see FractalCanvas
 * @see Fractal#createAffineImage(int)
 * @see Fractal#createJuliaImage(int)
 */
public final class FractalFactory {
  private static Description sierpinski;
  private static Description barnsley;
  private static Description juliaSet1;
  private static Description juliaSet2;
  private static Description juliaSet3;
  private static Description juliaSet4;
  private static Description mandelbrot;

  public static final int DEFAULT_JULIA_ITERATION = 500;
  public static final int DEFAULT_AFFINE_ITERATION = 100000;
  private static final Logger logger = Logger.getLogger(FractalFactory.class.getName());

  private FractalFactory() {
    // private constructor to prevent instantiation
  }

  /**
   * Initializes the factory with the descriptions from the .txt files.
   *
   * <p>Directory of the .txt files transformations: src/main/resources/fractal. The files are read
   * by {@link FileHandler}. The descriptions are stored in the static fields of the factory.
   */
  public static void initializeDescriptions() {
    try {
      sierpinski = FileHandler.readToDescription("src/main/resources/fractal/affineSier.txt");
      barnsley = FileHandler.readToDescription("src/main/resources/fractal/affineBarn.txt");
      juliaSet1 = FileHandler.readToDescription("src/main/resources/fractal/juliaSet1.txt");
      juliaSet2 = FileHandler.readToDescription("src/main/resources/fractal/juliaSet2.txt");
      juliaSet3 = FileHandler.readToDescription("src/main/resources/fractal/juliaSet3.txt");
      juliaSet4 = FileHandler.readToDescription("src/main/resources/fractal/juliaSet4.txt");
      mandelbrot = FileHandler.readToDescription("src/main/resources/fractal/mandelbrot.txt");
    } catch (DescriptionException e) {
      logger.info("——FACTORY STAGE——");
      logger.info("Error reading from file: " + e.getMessage());
    }
  }

  /**
   * Creates a Sierpinski triangle fractal.
   *
   * <p>The fractal is created with the description from the static field of the factory. The
   * fractal canvas is created with the min and max coordinates from the description. The fractal is
   * created with the affine transformation.
   *
   * @param fractal The fractal to be updated.
   */
  public static void createSierpinski(Fractal fractal) {
    Description description = sierpinski;
    FractalCanvas fractalCanvas =
        new FractalCanvas(2000, 2000, description.getMinCoords(), description.getMaxCoords());

    fractal.setDescription(description);
    fractal.setFractalCanvas(fractalCanvas);
    fractal.createAffineImage(DEFAULT_AFFINE_ITERATION);
  }

  /**
   * Creates a Barnsley fern fractal.
   *
   * <p>The fractal is created with the description from the static field of the factory. The
   * fractal canvas is created with the min and max coordinates from the description. The fractal is
   * created with the affine transformation.
   *
   * @param fractal The fractal to be updated.
   */
  public static void createBarnsley(Fractal fractal) {
    Description description = barnsley;
    FractalCanvas fractalCanvas =
        new FractalCanvas(2000, 2000, description.getMinCoords(), description.getMaxCoords());

    fractal.setDescription(description);
    fractal.setFractalCanvas(fractalCanvas);
    fractal.createAffineImage(DEFAULT_AFFINE_ITERATION);
  }

  /**
   * Creates a Julia set fractal.
   *
   * <p>The fractal is created with the description from the static field of the factory. The
   * fractal canvas is created with the min and max coordinates from the description. The fractal is
   * created with the Julia transformation.
   *
   * @param fractal The fractal to be updated.
   * @param selectedJulia The selected Julia set.
   */
  public static void createJulia(Fractal fractal, String selectedJulia) {
    Description juliaSetName = switch (selectedJulia) {
      case "juliaSet1" -> juliaSet1;
      case "juliaSet2" -> juliaSet2;
      case "juliaSet3" -> juliaSet3;
      case "juliaSet4" -> juliaSet4;
      default -> throw new IllegalStateException("Unexpected value: " + selectedJulia);
    };
    FractalCanvas fractalCanvas = new FractalCanvas(
        800, 800, juliaSetName.getMinCoords(), juliaSetName.getMaxCoords());

    fractal.setDescription(juliaSetName);
    fractal.setFractalCanvas(fractalCanvas);
    fractal.createJuliaImage(DEFAULT_JULIA_ITERATION);
    logger.info("——FACTORY STAGE——");
    logger.info("Selected set: " + selectedJulia);
  }

  /**
   * Creates a Mandelbrot set fractal.
   *
   * <p>The fractal is created with the description from the static field of the factory. The
   * fractal canvas is created with the min and max coordinates from the description. The fractal is
   * created with the Julia transformation.
   *
   * @param fractal The fractal to be updated.
   */
  public static void createMandelbrot(Fractal fractal) {
    Description description = mandelbrot;
    FractalCanvas fractalCanvas =
        new FractalCanvas(800, 800, description.getMinCoords(), description.getMaxCoords());

    fractal.setDescription(description);
    fractal.setFractalCanvas(fractalCanvas);
    fractal.createMandelImage(DEFAULT_JULIA_ITERATION);
  }

  /**
   * Creates a custom fractal.
   *
   * <p>The fractal is created with the description from the .txt file. The fractal canvas is
   * created with the min and max coordinates from the description. The fractal is created with the
   * affine or Julia transformation.
   *
   * @param fractal The fractal to be updated.
   * @throws DescriptionException If the description cannot be read from the file.
   */
  public static void createCustom(Fractal fractal) throws DescriptionException {
    Description description =
        FileHandler.readToDescription("src/main/resources/fractal/customFractal.txt");
    FractalCanvas fractalCanvas =
        new FractalCanvas(800, 800, description.getMinCoords(), description.getMaxCoords());

    fractal.setDescription(description);
    fractal.setFractalCanvas(fractalCanvas);

    if (description.getTransformationType().equals("Affine2D")) {
      fractal.createAffineImage(DEFAULT_AFFINE_ITERATION);
    }
    if (description.getTransformationType().equals("Julia")) {
      fractal.createJuliaImage(DEFAULT_JULIA_ITERATION);
    }
  }
}