package edu.ntnu.stud.model.linalg;

import org.apache.commons.math3.complex.Complex;

/**
 * Performs a Julia transformation on a 2D vector:
 * {@link JuliaTransform#transform(Vector2D)}
 *
 * <p>Implements {@link Transform2D}.
 *
 * @see Vector2D
 */
public class JuliaTransform implements Transform2D {
  private final Vector2D c;

  /**
   * Constructs a Julia transformation with the provided details.
   *
   * @param c    VectorComplex number to be used in the transformation z -> ± sqrt(z - c)
   */
  public JuliaTransform(Vector2D c) {
    this.c = c;
  }

  /**
   * Performs a Julia transformation on a 2D vector.
   *
   * @param z 2D vector used in the transformation z -> z^2 + c
   * @return Resulting 2D vector.
   */
  public Vector2D transform(Vector2D z) {
    Complex cCast = new Complex(c.getX(), c.getY());
    Complex zCast = new Complex(z.getX(), z.getY());
    Complex zSquared = zCast.pow(2);
    Complex sum = zSquared.add(cCast);
    return new Vector2D(sum.getReal(), sum.getImaginary());
  }

  public Vector2D getC() {
    return c;
  }

  @Override
  public String toString() {
    return c.toString();
  }
}