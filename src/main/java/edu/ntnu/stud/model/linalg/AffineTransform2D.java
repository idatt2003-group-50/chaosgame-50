package edu.ntnu.stud.model.linalg;

/**
 * Performs an affine transformation (A * x + b): {@link AffineTransform2D#transform(Vector2D)}.
 *
 * <p> Implements {@link Transform2D}. </p>
 *
 * @see Matrix2x2
 * @see Vector2D
 */
public class AffineTransform2D implements Transform2D {
  private final Matrix2x2 matrix;
  private final Vector2D vector;

  /**
   * Constructs an affine transformation with the provided details.
   *
   * @param matrix 2x2 matrix (A), represents the linear transformation.
   * @param vector 2D vector (b), represents the translation vector.
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    this.matrix = matrix;
    this.vector = vector;
  }

  /**
   * Performs an affine transformation on a 2D vector.
   *
   * @param point 2D vector (x), represents the point to be transformed.
   * @return The affine transformed 2D vector.
   */
  public Vector2D transform(Vector2D point) {
    return matrix.multiply(point).addition(vector);
  }

  public Matrix2x2 getMatrix() {
    return matrix;
  }

  public Vector2D getVector() {
    return vector;
  }

  @Override
  public String toString() {
    return matrix + ", " + vector;
  }
}