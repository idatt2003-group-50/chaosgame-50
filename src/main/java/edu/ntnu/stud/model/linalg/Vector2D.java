package edu.ntnu.stud.model.linalg;

/**
 * Creates a 2D vector. Provides methods for:
 *
 * <ul>
 *     <li> Getting the x-coordinate of the vector: {@link Vector2D#getX()}</li>
 *     <li> Getting the y-coordinate of the vector: {@link Vector2D#getY()}</li>
 *     <li> Adding together two vectors: {@link Vector2D#addition(Vector2D)}</li>
 * </ul>
 * Inherited by {@link VectorComplex}.
 */
public class Vector2D {
  private final double x;
  private final double y;

  /**
   * Constructs a 2D vector with the provided details.
   *
   * @param x coordinate of the vector.
   * @param y coordinate of the vector.
   */
  public Vector2D(double x, double y) {
    this.x = x;
    this.y = y;
  }

  /**
   * Adds together two vectors.
   *
   * @param other The vector to add to the current vector.
   * @return The resulting vector.
   */
  public Vector2D addition(Vector2D other) {
    return new Vector2D(x + other.getX(), y + other.getY());
  }

  /**
   * Subtracts one vector from another.
   *
   * @param other The vector to subtract from the current vector.
   * @return The resulting vector.
   */
  public Vector2D subtract(Vector2D other) {
    return new Vector2D(x - other.getX(), y - other.getY());
  }

  public double magnitude() {
    return Math.sqrt((x * x) + (y * y));
  }

  public double getX() {
    return x;
  }

  public double getY() {
    return y;
  }

  @Override
  public String toString() {
    return x + ", " + y;
  }

  public Vector2D multiply(int i) {
    return new Vector2D(x * i, y * i);
  }
}