package edu.ntnu.stud.model.linalg;

/**
 * Defines a method for transforming a 2D vector.
 *
 * <p>Implemented by:
 *
 * <ul>
 *   <li>{@link AffineTransform2D}
 *   <li>{@link JuliaTransform}
 * </ul>
 *
 * @see Vector2D
 */
public interface Transform2D {
  Vector2D transform(Vector2D point);
}