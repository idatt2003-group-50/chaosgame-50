package edu.ntnu.stud.model.linalg;

/**
 * Creates a 2x2 matrix. Provides methods for:
 *
 * <ul>
 *   <li>Multiplying a 2x2 matrix with a 2D vector: {@link Matrix2x2#multiply(Vector2D)}
 * </ul>
 */
public class Matrix2x2 {
  private final double a00;
  private final double a01;
  private final double a10;
  private final double a11;

  /**
   * Constructs a 2x2 matrix with the provided details.
   *
   * @param a00 The element at position (0,0) in the matrix.
   * @param a01 The element at position (0,1) in the matrix.
   * @param a10 The element at position (1,0) in the matrix.
   * @param a11 The element at position (1,1) in the matrix.
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  /**
   * Multiplies a 2x2 matrix with a 2D vector.
   *
   * @param vector The 2D vector to multiply with the matrix.
   * @return The resulting 2D vector.
   */
  public Vector2D multiply(Vector2D vector) {
    return new Vector2D(
        a00 * vector.getX() + a01 * vector.getY(),
        a10 * vector.getX() + a11 * vector.getY());
  }

  // Getters
  public double getA00() {
    return a00;
  }

  public double getA01() {
    return a01;
  }

  public double getA10() {
    return a10;
  }

  public double getA11() {
    return a11;
  }

  @Override
  public String toString() {
    return a00 + ", " + a01 + ", " + a10 + ", " + a11;
  }
}