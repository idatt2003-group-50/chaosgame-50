# Chaos Game
JavaFX application providing representations of the chaotic mathematical patterns found in fractals.
Allows representation of the Mandelbrot set, Sierpinski Triangle, Barnsley Fern, and several Julia sets.
The user may choose between predefined data for these fractals, or set their own transformations to create custom fractals.
## Running the application
The application may be launched by using the below command:
```bash
mvn javafx:run
```